package com.splwg.cm.domain.admin.financial.test;

import java.math.BigInteger;

import com.splwg.base.api.batch.SubmissionParameters;
import com.splwg.base.api.datatypes.Date;
import com.splwg.base.domain.batch.batchControl.BatchControl;
import com.splwg.base.domain.batch.batchRun.BatchRun;
import com.splwg.base.domain.common.language.Language_Id;
import com.splwg.base.domain.security.user.User_Id;
import com.splwg.base.support.sql.FunctionReplacerHelper;

import ru.ibs.test.framework.algorithm.AlienBatchTestCase;

import com.splwg.base.api.lookup.CustomizationOwnerLookup;
import com.splwg.cm.domain.financial.CmPeni_Batch;
public class CmPeni_BatchTest extends AlienBatchTestCase 
{


	@Override
	protected SubmissionParameters setupRun(SubmissionParameters paramSubmissionParameters) {
		FunctionReplacerHelper.enableNoDBSpecificFunctionValidation(false);

        BatchControl batchControl = createNewBatchControl(CmPeni_Batch.class, CustomizationOwnerLookup.constants.CM);
        
        paramSubmissionParameters.setBatchControlId(batchControl.getId());
        paramSubmissionParameters.setUserId(new User_Id("VMOGLAN"));
        paramSubmissionParameters.setLanguageId(new Language_Id("RUS"));
        paramSubmissionParameters.setProcessDate(new Date(2016, 04, 30));

        addSoftParameter(batchControl, paramSubmissionParameters, "MAX_ERRORS", "100", false);
        addSoftParameter(batchControl, paramSubmissionParameters, "RECALC-SW", "N", false);
        addSoftParameter(batchControl, paramSubmissionParameters, "BEGIN-DT", "01-04-2016", true);
        addSoftParameter(batchControl, paramSubmissionParameters, "END-DT", "30-04-2016", true);
        addSoftParameter(batchControl, paramSubmissionParameters, "BF-CD", "NBM-RATE", true);
        
        addSoftParameter(batchControl, paramSubmissionParameters, "CHAR-TYPE-ACCT-CD", "PN-BF", false);
        
        addSoftParameter(batchControl, paramSubmissionParameters, "CHAR-TYPE-CL-SD-CD", "PN-S-DT", false);
        addSoftParameter(batchControl, paramSubmissionParameters, "CHAR-TYPE-CL-ED-CD", "PN-E-DT", false);
        addSoftParameter(batchControl, paramSubmissionParameters, "CHAR-TYPE-CL-RT-CD", "PN-RATE", false);
        addSoftParameter(batchControl, paramSubmissionParameters, "CHAR-TYPE-CL-SA-CD", "PN-SA", false);
        addSoftParameter(batchControl, paramSubmissionParameters, "CHAR-TYPE-CL-AMT-CD", "PN-BASE", false);
                
        addSoftParameter(batchControl, paramSubmissionParameters, "CHAR-TYPE-SAT-CD", "SA-CAT", true);
        addSoftParameter(batchControl, paramSubmissionParameters, "CHAR-VAL-SAT", "PENI", true);
        addSoftParameter(batchControl, paramSubmissionParameters, "CHAR-TYPE-CPN-CD", "PN-P-SW", true);
        //addSoftParameter(batchControl, paramSubmissionParameters, "CHAR-TYPE-ADJ-DUE", "", false);
        addSoftParameter(batchControl, paramSubmissionParameters, "ACCT-ID", "0485820000", false);

        
        System.out.println(">>>>>>>>>>>>>>>START<<<<<<<<<<<<<<<<<");
        
        CmPeni_Batch.test = true;
        CmPeni_Batch.logQuery = false; 
        //ComponentContainer coponentContainer = ContextHolder.getContext().getContainer();
        return paramSubmissionParameters;
	}

	@Override
	protected void validateResults(BatchRun arg0) {
		System.out.println("validateResults is invoked");
		System.out.println("Обработано "+arg0.computeStatistics().getRecordsProcessed()+" записей из которых ошибочных "+arg0.computeStatistics().getRecordsInError());
	}

}
