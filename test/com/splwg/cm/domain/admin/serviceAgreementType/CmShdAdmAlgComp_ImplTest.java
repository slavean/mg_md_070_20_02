package com.splwg.cm.domain.admin.serviceAgreementType;


import java.util.ArrayList;
import java.util.List;

import ai.labs.algorithm.AlgorithmUtils;

import com.splwg.base.domain.todo.toDoEntry.ToDoEntry;
import com.splwg.base.domain.todo.toDoEntry.ToDoEntry_Id;
import com.splwg.ccb.domain.financial.financialTransaction.entity.FinancialTransaction;
import com.splwg.ccb.domain.financial.financialTransaction.entity.FinancialTransaction_Id;
import com.splwg.cm.domain.admin.serviceAgreementType.CmShdAdmAlgComp_Impl;

import ru.ibs.test.framework.algorithm.AlgorithmTestCase;


public class CmShdAdmAlgComp_ImplTest extends AlgorithmTestCase<CmShdAdmAlgComp_Impl> {

	private CmShdAdmAlgComp_Impl algorithm;
	private List<String> parameters = new ArrayList<String>();

	@Override
	protected Class<CmShdAdmAlgComp_Impl> getAlgorithmImplementationClass() {
		return CmShdAdmAlgComp_Impl.class;
	}

	public void testAlgorithm() 
	{
		parameters.clear();
		parameters.add("TE2020"); 
		parameters.add("25");

		algorithm = createAlgorithmCobol(parameters);
		FinancialTransaction_Id ftId = new FinancialTransaction_Id("562457067280");
		algorithm.setFinancialTransaction(ftId.getEntity());		
		algorithm.invoke();
	}
	
	/*public void testAlgorithmBad()
	{
		parameters.clear();
		parameters.add("FLG-AVSC"); 
		parameters.add("31s");
		algorithm = createAlgorithmCobol(parameters);
		//algorithm.setFinancialTransaction(null);		
		algorithm.invoke();		
	}*/
}
