package com.splwg.cm.domain.financial;

import com.ibm.icu.math.BigDecimal;
import com.splwg.base.api.datatypes.Date;
import com.splwg.base.api.datatypes.DateFormat;

public class TemplateRow {
	Date date;
	BigDecimal base;
	String rate;
	String saId;
	
	public String toString(){
		return "["+date.toString(new DateFormat("dd-MM-yyyy"))+", "+ base + ", "+rate+", "+saId+"]";
	}

	public TemplateRow(Date date, BigDecimal base, String rate, String saId) {
		super();
		this.date = date;
		this.base = base;
		this.rate = rate;
		this.saId = saId;
	}


	
}
