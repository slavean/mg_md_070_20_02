package com.splwg.cm.domain.financial;

//import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

import com.ibm.icu.math.BigDecimal;
import com.splwg.base.api.Query;
import com.splwg.base.api.QueryIterator;
import com.splwg.base.api.batch.JobWork;
import com.splwg.base.api.batch.RunAbortedException;
import com.splwg.base.api.batch.StandardCommitStrategy;
import com.splwg.base.api.batch.ThreadAbortedException;
import com.splwg.base.api.batch.ThreadExecutionStrategy;
import com.splwg.base.api.batch.ThreadWorkUnit;
import com.splwg.base.api.batch.ThreadWorker;
import com.splwg.base.api.datatypes.Bool;
import com.splwg.base.api.datatypes.Date;
import com.splwg.base.api.datatypes.DateFormat;
import com.splwg.base.api.datatypes.DateFormatParseException;
import com.splwg.base.api.datatypes.Money;
import com.splwg.base.api.datatypes.TimeInterval;
import com.splwg.base.api.lookup.MessageSeverityLookup;
import com.splwg.base.api.lookup.ThreadStatusLookup;
import com.splwg.base.api.sql.PreparedStatement;
import com.splwg.base.api.sql.SQLResultRow;
import com.splwg.base.domain.batch.batchRun.BatchThread;
import com.splwg.base.domain.common.characteristicType.CharacteristicEntity_Id;
import com.splwg.base.domain.common.characteristicType.CharacteristicType_Id;
import com.splwg.base.domain.common.characteristicType.CharacteristicValue_Id;
import com.splwg.base.domain.common.lookup.LookupField_Id;
import com.splwg.base.domain.common.lookup.LookupValue_Id;
import com.splwg.base.support.context.Session;
import com.splwg.base.support.context.SessionHolder;
import com.splwg.ccb.api.lookup.BillableChargeStatusLookup;
import com.splwg.ccb.api.lookup.CharacteristicEntityLookup;
import com.splwg.ccb.api.lookup.CustomerReadLookup;
import com.splwg.ccb.api.lookup.ServiceAgreementStatusLookup;
import com.splwg.ccb.domain.adjustment.adjustment.entity.Adjustment;
import com.splwg.ccb.domain.adjustment.adjustment.entity.AdjustmentCharacteristic;
import com.splwg.ccb.domain.adjustment.adjustment.entity.AdjustmentCharacteristic_DTO;
import com.splwg.ccb.domain.adjustment.adjustment.entity.Adjustment_Id;
import com.splwg.ccb.domain.admin.billFactor.entity.BillFactor_Id;
import com.splwg.ccb.domain.admin.serviceAgreementType.entity.ServiceAgreementType_Id;
import com.splwg.ccb.domain.billing.billableCharge.entity.BillableCharge;
import com.splwg.ccb.domain.billing.billableCharge.entity.BillableChargeLine;
import com.splwg.ccb.domain.billing.billableCharge.entity.BillableChargeLineCharacteristic;
import com.splwg.ccb.domain.billing.billableCharge.entity.BillableChargeLineCharacteristic_DTO;
import com.splwg.ccb.domain.billing.billableCharge.entity.BillableChargeLineCharacteristic_Id;
import com.splwg.ccb.domain.billing.billableCharge.entity.BillableChargeLine_DTO;
import com.splwg.ccb.domain.billing.billableCharge.entity.BillableChargeLine_Id;
import com.splwg.ccb.domain.billing.billableCharge.entity.BillableCharge_DTO;
import com.splwg.ccb.domain.billing.billableCharge.entity.BillableCharge_Id;
import com.splwg.ccb.domain.customerinfo.account.entity.Account_Id;
import com.splwg.ccb.domain.customerinfo.serviceAgreement.entity.ServiceAgreement;
import com.splwg.ccb.domain.customerinfo.serviceAgreement.entity.ServiceAgreement_DTO;
import com.splwg.ccb.domain.customerinfo.serviceAgreement.entity.ServiceAgreement_Id;
import com.splwg.cm.domain.common.CharacteristicHelper;
import com.splwg.shared.common.ServerMessage;
import com.splwg.shared.logging.Logger;
import com.splwg.shared.logging.LoggerFactory;
import com.splwg.xai.adapters.siebelxmlgateway.SetService;

/**
 * @batch CMPENI
 * @batchDescription "ENG" "Расчёт пени" "Пакетный процесс создаёт сторонние начисления для РДО Пени лицевых счетов с заданной категорией абонента. В случае отсутствия РДО оно будет создано. Сторонние начисления содержат начисления пени в строках расчёта"
 * @batchDescription "RUS" "Расчёт пени" "Пакетный процесс создаёт сторонние начисления для РДО Пени лицевых счетов с заданной категорией абонента. В случае отсутствия РДО оно будет создано. Сторонние начисления содержат начисления пени в строках расчёта"
 * @batchDescription "RUM" "Расчёт пени" "Пакетный процесс создаёт сторонние начисления для РДО Пени лицевых счетов с заданной категорией абонента. В случае отсутствия РДО оно будет создано. Сторонние начисления содержат начисления пени в строках расчёта"
 * @batchParameter 10 "MAX_ERRORS" "100" N
 * @batchParameterDescription "ENG" "Допустимое количество ошибок до окончания процесса" "Допустимое количество ошибок до окончания процесса"
 * @batchParameterDescription "RUS" "Допустимое количество ошибок до окончания процесса" "Допустимое количество ошибок до окончания процесса"
 * @batchParameterDescription "RUM" "Допустимое количество ошибок до окончания процесса" "Допустимое количество ошибок до окончания процесса"
 * @batchParameter 20 "RECALC-SW" "N" N
 * @batchParameterDescription "ENG" "Выполнить перерасчёт" "Выполнить перерасчёт"
 * @batchParameterDescription "RUS" "Выполнить перерасчёт" "Выполнить перерасчёт"
 * @batchParameterDescription "RUM" "Выполнить перерасчёт" "Выполнить перерасчёт"
 * @batchParameter 30 "BEGIN-DT" "" Y
 * @batchParameterDescription "ENG" "Дата начала расчёта Пени (ДД-ММ-ГГГГ)" "Дата начала расчёта Пени (ДД-ММ-ГГГГ)"
 * @batchParameterDescription "RUS" "Дата начала расчёта Пени (ДД-ММ-ГГГГ)" "Дата начала расчёта Пени (ДД-ММ-ГГГГ)"
 * @batchParameterDescription "RUM" "Дата начала расчёта Пени (ДД-ММ-ГГГГ)" "Дата начала расчёта Пени (ДД-ММ-ГГГГ)"
 * @batchParameter 40 "END-DT" "" Y
 * @batchParameterDescription "ENG" "Дата окончания расчёта Пени (ДД-ММ-ГГГГ)" "Дата окончания расчёта Пени (ДД-ММ-ГГГГ)"
 * @batchParameterDescription "RUS" "Дата окончания расчёта Пени (ДД-ММ-ГГГГ)" "Дата окончания расчёта Пени (ДД-ММ-ГГГГ)"
 * @batchParameterDescription "RUM" "Дата окончания расчёта Пени (ДД-ММ-ГГГГ)" "Дата окончания расчёта Пени (ДД-ММ-ГГГГ)"
 * @batchParameter 50 "BF-CD" "" Y
 * @batchParameterDescription "ENG" "Фактор расчёта ставки пени" "Фактор расчёта ставки пени"
 * @batchParameterDescription "RUS" "Фактор расчёта ставки пени" "Фактор расчёта ставки пени"
 * @batchParameterDescription "RUM" "Фактор расчёта ставки пени" "Фактор расчёта ставки пени"
 * @batchParameter 60 "CHAR-TYPE-ACCT-CD" "" N
 * @batchParameterDescription "ENG" "Тип характеристики лицевого счёта «Фактор расчёта»" "Тип характеристики лицевого счёта «Фактор расчёта»"
 * @batchParameterDescription "RUS" "Тип характеристики лицевого счёта «Фактор расчёта»" "Тип характеристики лицевого счёта «Фактор расчёта»"
 * @batchParameterDescription "RUM" "Тип характеристики лицевого счёта «Фактор расчёта»" "Тип характеристики лицевого счёта «Фактор расчёта»"
 * @batchParameter 70 "CHAR-TYPE-CL-SD-CD" "" N
 * @batchParameterDescription "ENG" "Тип характеристики строки расчёта Дата начала периода" "Тип характеристики строки расчёта Дата начала периода"
 * @batchParameterDescription "RUS" "Тип характеристики строки расчёта Дата начала периода" "Тип характеристики строки расчёта Дата начала периода"
 * @batchParameterDescription "RUM" "Тип характеристики строки расчёта Дата начала периода" "Тип характеристики строки расчёта Дата начала периода" 
 * @batchParameter 80 "CHAR-TYPE-CL-ED-CD" "" N
 * @batchParameterDescription "ENG" "Тип хар. строки расчёта Дата окончания периода" "Тип характеристики строки расчёта Дата окончания периода"
 * @batchParameterDescription "RUS" "Тип хар. строки расчёта Дата окончания периода" "Тип характеристики строки расчёта Дата окончания периода"
 * @batchParameterDescription "RUM" "Тип хар. строки расчёта Дата окончания периода" "Тип характеристики строки расчёта Дата окончания периода"
 * @batchParameter 90 "CHAR-TYPE-CL-RT-CD" "" N
 * @batchParameterDescription "ENG" "Тип характеристики строки расчёта Ставка" "Тип характеристики строки расчёта Ставка"
 * @batchParameterDescription "RUS" "Тип характеристики строки расчёта Ставка" "Тип характеристики строки расчёта Ставка"
 * @batchParameterDescription "RUM" "Тип характеристики строки расчёта Ставка" "Тип характеристики строки расчёта Ставка"
 * @batchParameter 100 "CHAR-TYPE-CL-SA-CD" "" N
 * @batchParameterDescription "ENG" "Тип характеристики строки расчёта РДО" "Тип характеристики строки расчёта РДО"
 * @batchParameterDescription "RUS" "Тип характеристики строки расчёта РДО" "Тип характеристики строки расчёта РДО"
 * @batchParameterDescription "RUM" "Тип характеристики строки расчёта РДО" "Тип характеристики строки расчёта РДО" 
 * @batchParameter 110 "CHAR-TYPE-CL-AMT-CD" "" N
 * @batchParameterDescription "ENG" "Тип характеристики строки расчёта Базовая сумма" "Тип характеристики строки расчёта Базовая сумма"
 * @batchParameterDescription "RUS" "Тип характеристики строки расчёта Базовая сумма" "Тип характеристики строки расчёта Базовая сумма"
 * @batchParameterDescription "RUM" "Тип характеристики строки расчёта Базовая сумма" "Тип характеристики строки расчёта Базовая сумма"  
 * @batchParameter 120 "CHAR-TYPE-SAT-CD" "" Y
 * @batchParameterDescription "ENG" "Тип характеристики типа РДО «Категория типа РДО»" "Тип характеристики типа РДО «Категория типа РДО»"
 * @batchParameterDescription "RUS" "Тип характеристики типа РДО «Категория типа РДО»" "Тип характеристики типа РДО «Категория типа РДО»"
 * @batchParameterDescription "RUM" "Тип характеристики типа РДО «Категория типа РДО»" "Тип характеристики типа РДО «Категория типа РДО»"   
 * @batchParameter 130 "CHAR-VAL-SAT" "" Y
 * @batchParameterDescription "ENG" "Значение Типа характеристики «Категория типа РДО»" "Значение Типа характеристики «Категория типа РДО»"
 * @batchParameterDescription "RUS" "Значение Типа характеристики «Категория типа РДО»" "Значение Типа характеристики «Категория типа РДО»"
 * @batchParameterDescription "RUM" "Значение Типа характеристики «Категория типа РДО»" "Значение Типа характеристики «Категория типа РДО»" 
 * @batchParameter 140 "CHAR-TYPE-CPN-CD" "" Y
 * @batchParameterDescription "ENG" "Тип хар. типа РДО «Участвует в расчёте Пени?»" "Тип характеристики типа РДО «Участвует в расчёте Пени?»"
 * @batchParameterDescription "RUS" "Тип хар. типа РДО «Участвует в расчёте Пени?»" "Тип характеристики типа РДО «Участвует в расчёте Пени?»"
 * @batchParameterDescription "RUM" "Тип хар. типа РДО «Участвует в расчёте Пени?»" "Тип характеристики типа РДО «Участвует в расчёте Пени?»" 
 * @batchParameter 150 "CHAR-TYPE-ADJ-DUE" "" N
 * @batchParameterDescription "ENG" "Тип хар. корр. переопределения даты к оплате" "Тип характеристики корректировки переопределения даты к оплате"
 * @batchParameterDescription "RUS" "Тип хар. корр. переопределения даты к оплате" "Тип характеристики корректировки переопределения даты к оплате"
 * @batchParameterDescription "RUM" "Тип хар. корр. переопределения даты к оплате" "Тип характеристики корректировки переопределения даты к оплате" 
 * @batchParameter 160 "ACCT-ID" "" N
 * @batchParameterDescription "ENG" "Значение идентификатора" "Значение идентификатора"
 * @batchParameterDescription "RUS" "Значение идентификатора" "Значение идентификатора"
 * @batchParameterDescription "RUM" "Значение идентификатора" "Значение идентификатора"
 *   
 * @author vmoglan
 * @BatchJob (modules = { }, 
 * rerunnable = false, 
 * multiThreaded = true,
 * softParameters = { @BatchJobSoftParameter (name = MAX_ERRORS, required = false, type = integer)  
 * , @BatchJobSoftParameter (name = RECALC-SW, required = false, type = string)  
 * , @BatchJobSoftParameter (name = BEGIN-DT, required = true, type = string)  
 * , @BatchJobSoftParameter (name = END-DT, required = true, type = string)
 * , @BatchJobSoftParameter (name = BF-CD, required = true, type = string)
 * , @BatchJobSoftParameter (name = CHAR-TYPE-ACCT-CD, required = false, type = string)
 * , @BatchJobSoftParameter (name = CHAR-TYPE-CL-SD-CD, required = false, type = string)
 * , @BatchJobSoftParameter (name = CHAR-TYPE-CL-ED-CD, required = false, type = string) 
 * , @BatchJobSoftParameter (name = CHAR-TYPE-CL-RT-CD, required = false, type = string)
 * , @BatchJobSoftParameter (name = CHAR-TYPE-CL-SA-CD, required = false, type = string)  
 * , @BatchJobSoftParameter (name = CHAR-TYPE-CL-AMT-CD, required = false, type = string)
 * , @BatchJobSoftParameter (name = CHAR-TYPE-SAT-CD, required = true, type = string)
 * , @BatchJobSoftParameter (name = CHAR-VAL-SAT, required = true, type = string)
 * , @BatchJobSoftParameter (name = CHAR-TYPE-CPN-CD, required = true, type = string)
 * , @BatchJobSoftParameter (name = CHAR-TYPE-ADJ-DUE, required = false, type = string) 
 * , @BatchJobSoftParameter (name = ACCT-ID, required = false, type = string)
 * } 
 * )
 */

public class CmPeni_Batch extends CmPeni_Batch_Gen {



	static Logger logger = LoggerFactory.getLogger(CmPeni_Batch.class);
	
	public static boolean test = false;
	public static boolean logQuery = false;
	
	static int nrErrors = 0;	
	static int maxErrors = 0;
	private static String recalcSw;
	private static String beginDt;
	private static String endDt;
	private static String bfCd;
	private static String charTypeAcctCd;
	private static String charTypeClSdCd;
	private static String charTypeClEdCd;
	private static String charTypeClRtCd;
	private static String charTypeClSaCd;
	private static String charTypeClAmtCd;
	private static String charTypeSatCd;
	private static String charValSat;
	private static String charTypeCpnCd;
	private static String charTypeAdjDue;
	private String acctId;
//	private static Date processDT;
	

	@Override
	public Class<? extends ThreadWorker> getThreadWorkerClass() {

		return CmPeni_BatchWorker.class;
	}	

	
	@SuppressWarnings("deprecation")
	@Override
	public void validateSoftParameters(boolean isNewRun) {

		
/*		if (getParameters().getTEST().equals("Y"))
			CmPeni_Batch.test = true;
		else CmPeni_Batch.test = false;

		logger.info("TEST = ["+ CmPeni_Batch.test+"]");
*/
				
		if (getParameters().getMAX_ERRORS()!=null)
			maxErrors = getParameters().getMAX_ERRORS().intValue();
		else 
			maxErrors = 100;
		
		logger.info("MAX_ERRORS = ["+maxErrors+"]");
		
		
		Date processDT = getParameters().getProcessDate();
		
		recalcSw =  getParameters().getRECALC_SW();
		logger.info("RECALC_SW = ["+recalcSw+"]");
		
		if ((recalcSw != null) && ((!recalcSw.toUpperCase().equals("Y")) && (!recalcSw.toUpperCase().equals("N"))))
			addError(CmPeni_Batch_MessageRepository.incorrectRecalcSw(recalcSw));
		
		DateFormat df = new DateFormat("dd-MM-yyyy");
		
		beginDt = getParameters().getBEGIN_DT();
		logger.info("BEGIN_DT = ["+beginDt+"]");
		
		if (!Date.isValidDateString(beginDt, df))
				addError(CmPeni_Batch_MessageRepository.incorrectBeginDT(beginDt));			
		try {
			if (Date.fromString(beginDt, df).isAfter(processDT))
			{
				addError(CmPeni_Batch_MessageRepository.beginDtNotExceed(beginDt));	
			}
		} catch (DateFormatParseException e) {
			e.printStackTrace();
			addError(CmPeni_Batch_MessageRepository.incorrectBeginDT(beginDt));
		}
		
		endDt = getParameters().getEND_DT();
		logger.info("END_DT = ["+endDt+"]");
		
		if (!Date.isValidDateString(endDt, df))
			addError(CmPeni_Batch_MessageRepository.incorrectEndDT(endDt));			
		try {
			if (Date.fromString(endDt, df).isAfter(processDT))
			{
				addError(CmPeni_Batch_MessageRepository.endDtNotExceed(endDt));	
			}
		} catch (DateFormatParseException e) {
			e.printStackTrace();
			addError(CmPeni_Batch_MessageRepository.incorrectEndDT(endDt));	
		}

		bfCd = getParameters().getBF_CD();
		logger.info("BF_CD = ["+bfCd+"]");
		
		BillFactor_Id bf_id = new BillFactor_Id(bfCd);
		if (bf_id.getEntity()==null)
			addError(CmPeni_Batch_MessageRepository.noBfCd(bfCd));	
				
		charTypeAcctCd = getParameters().getCHAR_TYPE_ACCT_CD();
		logger.info("CHAR_TYPE_ACCT_CD = ["+charTypeAcctCd+"]");
		
		if (!isBlankOrNull(charTypeAcctCd))
		{
			CharacteristicType_Id charType = new CharacteristicType_Id(charTypeAcctCd); 
			if (charType.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.noCharType(charTypeAcctCd));
			
			CharacteristicEntity_Id charEntityId = new CharacteristicEntity_Id(charType,  CharacteristicEntityLookup.constants.ACCOUNT /*(CharacteristicEntityLookup) charEnityLookupId.getEntity().asLookup()*/);
						
			
			
			if (charEntityId.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.notExistsCharObject(charTypeAcctCd, CharacteristicEntityLookup.constants.ACCOUNT.getLookupValue().getDTO().getLanguageDescription()));
		}
		
		charTypeClSdCd = getParameters().getCHAR_TYPE_CL_SD_CD();
		logger.info("CHAR_TYPE_CL_SD_CD = ["+charTypeClSdCd+"]");
		
		if (!isBlankOrNull(charTypeClSdCd))
		{
			CharacteristicType_Id charType = new CharacteristicType_Id(charTypeClSdCd); 
			if (charType.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.noCharType(charTypeClSdCd));
			
			LookupField_Id lfId = new LookupField_Id("CHAR_ENTITY_FLG");
			LookupValue_Id charEnityLookupId =  new LookupValue_Id(lfId, "BCHG");
			CharacteristicEntity_Id charEntityId = new CharacteristicEntity_Id(charType, (CharacteristicEntityLookup) charEnityLookupId.getEntity().asLookup());
			
			if (charEntityId.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.notExistsCharObject(charTypeClSdCd, charEnityLookupId.getEntity().getDTO().getLanguageDescription()));				
		}
		
		
		charTypeClEdCd = getParameters().getCHAR_TYPE_CL_ED_CD();
		logger.info("CHAR_TYPE_CL_ED_CD = ["+charTypeClEdCd+"]");
		
		if (!isBlankOrNull(charTypeClEdCd))
		{
			CharacteristicType_Id charType = new CharacteristicType_Id(charTypeClEdCd); 
			if (charType.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.noCharType(charTypeClEdCd));
			
			LookupField_Id lfId = new LookupField_Id("CHAR_ENTITY_FLG");
			LookupValue_Id charEnityLookupId =  new LookupValue_Id(lfId, "BCHG");
			CharacteristicEntity_Id charEntityId = new CharacteristicEntity_Id(charType, (CharacteristicEntityLookup) charEnityLookupId.getEntity().asLookup());
			
			if (charEntityId.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.notExistsCharObject(charTypeClEdCd, charEnityLookupId.getEntity().getDTO().getLanguageDescription()));
		}		
		
		charTypeClRtCd = getParameters().getCHAR_TYPE_CL_RT_CD();
		logger.info("CHAR_TYPE_CL_RT_CD = ["+charTypeClRtCd+"]");
		
		if (!isBlankOrNull(charTypeClRtCd))
		{
			CharacteristicType_Id charType = new CharacteristicType_Id(charTypeClRtCd); 
			if (charType.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.noCharType(charTypeClRtCd));
			
			LookupField_Id lfId = new LookupField_Id("CHAR_ENTITY_FLG");
			LookupValue_Id charEnityLookupId =  new LookupValue_Id(lfId, "BCHG");
			CharacteristicEntity_Id charEntityId = new CharacteristicEntity_Id(charType, (CharacteristicEntityLookup) charEnityLookupId.getEntity().asLookup());
			
			if (charEntityId.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.notExistsCharObject(charTypeClRtCd, charEnityLookupId.getEntity().getDTO().getLanguageDescription()));
		}			
		
		
		charTypeClSaCd = getParameters().getCHAR_TYPE_CL_SA_CD();
		logger.info("CHAR_TYPE_CL_SA_CD = ["+charTypeClSaCd+"]");
		
		if (!isBlankOrNull(charTypeClSaCd))
		{
			CharacteristicType_Id charType = new CharacteristicType_Id(charTypeClSaCd); 
			if (charType.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.noCharType(charTypeClSaCd));
			
			LookupField_Id lfId = new LookupField_Id("CHAR_ENTITY_FLG");
			LookupValue_Id charEnityLookupId =  new LookupValue_Id(lfId, "BCHG");
			CharacteristicEntity_Id charEntityId = new CharacteristicEntity_Id(charType, (CharacteristicEntityLookup) charEnityLookupId.getEntity().asLookup());
			
			if (charEntityId.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.notExistsCharObject(charTypeClSaCd, charEnityLookupId.getEntity().getDTO().getLanguageDescription()));
		}	
		
		charTypeClAmtCd = getParameters().getCHAR_TYPE_CL_AMT_CD();
		logger.info("CHAR_TYPE_CL_AMT_CD = ["+charTypeClAmtCd+"]");
		
		if (!isBlankOrNull(charTypeClAmtCd))
		{
			CharacteristicType_Id charType = new CharacteristicType_Id(charTypeClAmtCd); 
			if (charType.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.noCharType(charTypeClAmtCd));
			
			LookupField_Id lfId = new LookupField_Id("CHAR_ENTITY_FLG");
			LookupValue_Id charEnityLookupId =  new LookupValue_Id(lfId, "BCHG");
			CharacteristicEntity_Id charEntityId = new CharacteristicEntity_Id(charType, (CharacteristicEntityLookup) charEnityLookupId.getEntity().asLookup());
			
			if (charEntityId.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.notExistsCharObject(charTypeClAmtCd, charEnityLookupId.getEntity().getDTO().getLanguageDescription()));
				
		}
		
		charTypeSatCd = getParameters().getCHAR_TYPE_SAT_CD();
		logger.info("CHAR_TYPE_SAT_CD = ["+charTypeSatCd+"]");
		
		if (!isBlankOrNull(charTypeSatCd))
		{
			CharacteristicType_Id charType = new CharacteristicType_Id(charTypeSatCd); 
			if (charType.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.noCharType(charTypeSatCd));
			

			CharacteristicEntity_Id charEntityId = new CharacteristicEntity_Id(charType, CharacteristicEntityLookup.constants.SERVICE_AGREEMENT_TYPE);
			
			if (charEntityId.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.notExistsCharObject(charTypeSatCd, CharacteristicEntityLookup.constants.SERVICE_AGREEMENT_TYPE.getLookupValue().getDTO().getLanguageDescription()));
		}		
		
		charValSat = getParameters().getCHAR_VAL_SAT();
		logger.info("CHAR_VAL_SAT = ["+charValSat+"]");
		
		if (!isBlankOrNull(charValSat))
		{
		
			CharacteristicType_Id charType = new CharacteristicType_Id(charTypeSatCd); 
			CharacteristicValue_Id  cv = new CharacteristicValue_Id(charType.getEntity(), charValSat);
			
			if (cv.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.notExistsCharValSat(charValSat, charTypeSatCd));

		}		
		
		charTypeCpnCd = getParameters().getCHAR_TYPE_CPN_CD();
		logger.info("CHAR_TYPE_CPN_CD = ["+charTypeCpnCd+"]");
		
		if (!isBlankOrNull(charTypeCpnCd))
		{
			CharacteristicType_Id charType = new CharacteristicType_Id(charTypeCpnCd); 
			if (charType.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.noCharType(charTypeCpnCd));
			
			CharacteristicEntity_Id charEntityId = new CharacteristicEntity_Id(charType, CharacteristicEntityLookup.constants.SERVICE_AGREEMENT_TYPE);
			
			if (charEntityId.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.notExistsCharObject(charTypeCpnCd,  CharacteristicEntityLookup.constants.SERVICE_AGREEMENT_TYPE.getLookupValue().getDTO().getLanguageDescription()));
		}
		
		
		charTypeAdjDue = getParameters().getCHAR_TYPE_ADJ_DUE();
		logger.info("CHAR_TYPE_ADJ_DUE = ["+charTypeAdjDue+"]");
		
		if (!isBlankOrNull(charTypeAdjDue))
		{
			CharacteristicType_Id charType = new CharacteristicType_Id(charTypeAdjDue); 
			if (charType.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.noCharType(charTypeAdjDue));
			
			CharacteristicEntity_Id charEntityId = new CharacteristicEntity_Id(charType, CharacteristicEntityLookup.constants.ADJUSTMENT_TYPE);
			
			if (charEntityId.getEntity()==null)
				addError(CmPeni_Batch_MessageRepository.notExistsCharObject(charTypeAdjDue,  CharacteristicEntityLookup.constants.ADJUSTMENT_TYPE.getLookupValue().getDTO().getLanguageDescription()));
		}		
		

		acctId = getParameters().getACCT_ID();
		logger.info("ACCT_ID = ["+acctId+"]");
	
		
		if (!isBlankOrNull(acctId))
		{		
			PreparedStatement psAcc = null;
			psAcc = createPreparedStatement("select distinct ACCT_ID from CI_ACCT where ACCT_ID='"+acctId+"' " );
			try
			{
			if ( (psAcc.list().size()==0) )
				addError(CmPeni_Batch_MessageRepository.notExistsAcctId(acctId));
			}
			finally
			{
			if (psAcc!=null)
				psAcc.close();
			}

		}	
		
		
		
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public JobWork getJobWork() {

		List<ThreadWorkUnit> units = new ArrayList<ThreadWorkUnit>();
		
		
		if (!isBlankOrNull(acctId))
		{				
			ThreadWorkUnit unit = new ThreadWorkUnit();
			unit.addSupplementalData("ACCT_ID", acctId);
			units.add(unit);
		}	
		else
		{
		
			StringBuilder sb = new StringBuilder();
		
			sb.append("select distinct acct.ACCT_ID from CI_ACCT acct \n");
			sb.append("join CI_SA sa on sa.ACCT_ID=acct.ACCT_ID \n");
			sb.append("join CI_SA_TYPE sat on sat.SA_TYPE_CD = sa.SA_TYPE_CD and sa.CIS_DIVISION=sat.CIS_DIVISION \n");
			sb.append("join CI_SA_TYPE_CHAR satc on satc.SA_TYPE_CD = sat.SA_TYPE_CD and trim(satc.CHAR_TYPE_CD) = trim(:CHAR_TYPE_CPN_CD)  \n");

	 
			String query = sb.toString();
			
			query = query.replaceAll(":CHAR_TYPE_CPN_CD", "'"+charTypeCpnCd+"'");
			PreparedStatement ps = null;
			
			try
			{

				if (logQuery) 
					logger.info("Accounts query = ["+query+"]");

				ps = createPreparedStatement(query);
				ps.setAutoclose(false);
				
				logger.info("Accounts list size = ["+ps.list().size()+"]");

				
				QueryIterator<SQLResultRow> accIterator = ps.iterate();
				while(accIterator.hasNext())
				{
					SQLResultRow row = accIterator.next();
					String accId = row.getString("ACCT_ID");				
					ThreadWorkUnit unit = new ThreadWorkUnit();
					unit.addSupplementalData("ACCT_ID", accId);
					units.add(unit);
				}
				accIterator.close();
			
			}
			finally
			{
				if (ps!=null)
					ps.close();
			}
		}

		
		return createJobWorkForThreadWorkUnitList(units);
	}
	


	public static class CmPeni_BatchWorker extends CmPeni_BatchWorker_Gen {

	    private int okNumb;
	    private int errNumb;
	    private Session session;
	    
	    
	    public CmPeni_BatchWorker()
	    {
      
	      this.okNumb = 0;
	      
	      this.errNumb = 0;

	    }
	    
		@SuppressWarnings("deprecation")
		@Override
		  public void finalizeThreadWork()
			  
				  throws ThreadAbortedException, RunAbortedException
			    {
			      Query<BatchThread> query = createQuery("from BatchThread th  where th.id.batchRun.id.batchControl.id = :id  and th.id.batchRun.id.batchNumber = :number  and th.id.batchRun.id.batchRerunNumber = :rerun  and th.threadStatus = :run");
			      
			      query.bindId("id", getBatchControlId());
			      query.bindBigInteger("number", getBatchNumber());
			      query.bindBigInteger("rerun", getBatchRerunNumber());
			      query.bindLookup("run", ThreadStatusLookup.constants.IN_PROGRESS);
			      
			      long listSize = query.listSize();
			      
			      CmPeni_Batch.logger.info("num of active threads = " + listSize);

			      ServerMessage mes = com.splwg.base.domain.batch.batchRun.MessageRepository.getRecordProcessInfo(this.okNumb+this.errNumb, this.errNumb, new TimeInterval(getBatchControlId().getEntity().getTimerInterval().longValue()));
			      CmPeni_Batch.logger.info(mes.getMessageText());
			      
			      logMessage(mes , MessageSeverityLookup.constants.INFORMATIONAL);
			      
			      /*
			      if (listSize == 1L)
			      {
			    	CmPeni_Batch.logger.info("Run compelete program");
			        CobolProgramCMPPUPFN cobolComponent = (CobolProgramCMPPUPFN)getDynamicComponent(CobolProgramCMPPUPFN.class);
			        cobolComponent.callCobol();
			      }
			      for (ServerMessage message : getMessagesForErrors())
			      {
			    	  CmPeni_Batch.logger.info("logMessage message = [" + message + "]");
			        logMessage(message, MessageSeverityLookup.constants.INFORMATIONAL);
			      }
			      */
			      //logMessage(CmPl03MessageRepository.threadResultMessage(this.okNumb, this.okAmt, this.errNumb, this.errAmt), MessageSeverityLookup.constants.INFORMATIONAL);
			    }

		@Override
		public ThreadExecutionStrategy createExecutionStrategy() {

			return new StandardCommitStrategy(this);
		}

		@SuppressWarnings("deprecation")
		@Override
		protected boolean executeWorkUnit(ThreadWorkUnit unit)
				throws ThreadAbortedException, RunAbortedException {
		
			
			 boolean success = true;
			 

			DateFormat df = new DateFormat("dd-MM-yyyy");
			Date bgDt = null;
			Date enDt = null;
			try {
				bgDt = Date.fromString(beginDt, df);
				enDt = Date.fromString(endDt, df);
				
			} catch (DateFormatParseException e) {
				
				e.printStackTrace();
			}
			
			String acctId = (String) unit.getSupplementallData("ACCT_ID");
			logger.info("ACCT_ID = ["+acctId+"]" );
			logger.info("Thread Number = ["+getBatchThreadNumber()+"]" );
			
			String savePointName = "CmPeni_BatchWorker: "+acctId;
			
			setSavepoint(savePointName);
			startChanges();
			
			StringBuilder sb = new StringBuilder();
			sb.append("select DISTINCT bch.BILLABLE_CHG_ID from CI_BILL_CHG bch  \n");
			sb.append("join CI_SA sa on sa.SA_ID = bch.SA_ID \n");
			sb.append("join CI_SA_TYPE sat on sat.SA_TYPE_CD = sa.SA_TYPE_CD and sa.CIS_DIVISION=sat.CIS_DIVISION \n");
			sb.append("join CI_SA_TYPE_CHAR satc on satc.SA_TYPE_CD = sat.SA_TYPE_CD and trim(satc.CHAR_TYPE_CD) = trim(:CHAR_TYPE_SAT_CD) and trim(satc.CHAR_VAL)=trim(:CHAR_VAL_SAT)  \n");
			sb.append("where sa.ACCT_ID = :ACCT_ID and bch.BILLABLE_CHG_STAT=10 and bch.END_DT = :END_DT \n");
				
			

			
			String queryBCHG = sb.toString();

			queryBCHG = queryBCHG.replaceAll(":CHAR_TYPE_SAT_CD", "'"+charTypeSatCd+"'");
			queryBCHG = queryBCHG.replaceAll(":CHAR_VAL_SAT", "'"+charValSat+"'");

			sb.setLength(0);
			sb.append("SELECT pp.START_DT from CI_PP pp where pp.ACCT_ID= :ACCT_ID and pp.PP_STAT_FLG = 20");
			String queryPP = sb.toString();


			sb.setLength(0);
			sb.append("select FR from  ( \n");
			sb.append("select EFFDT, CHAR_VAL_FK1 as FR from ci_acct_char ac where ac.ACCT_ID = :ACCT_ID and ac.EFFDT<= :BEGIN_DT and TRIM(ac.CHAR_TYPE_CD) = TRIM( :CHAR_TYPE_ACCT_CD ) \n");
			sb.append("order by EFFDT desc) TT where ROWNUM=1 \n");
									
			String queryBFAcc = sb.toString();
			queryBFAcc = queryBFAcc.replaceAll(":CHAR_TYPE_ACCT_CD", "'"+charTypeAcctCd+"'");

			
			PreparedStatement psBCHG = null, psBSC = null, psUpdateBCHG = null, psPP = null, 
					psFT = null, psFT1 = null, psBF = null, psBF1 = null, psTemplate = null, psSA = null, psSEG = null, psCorr = null, psPay = null, psSAPeni = null,
					psSATY = null, psBFAcc = null;;
			try
			{


				psBCHG = createPreparedStatement(queryBCHG);
				psBCHG.setAutoclose(false);
				psBCHG.bindId("ACCT_ID", new Account_Id(acctId));
				psBCHG.bindDate("END_DT", enDt);

				psPP = createPreparedStatement(queryPP);
				psPP.setAutoclose(false);
				
				
				psBFAcc = createPreparedStatement(queryBFAcc);
				psBFAcc.setAutoclose(false);

				
				if (logQuery)
					logger.info("psBCHG statement  = ["+psBCHG.toString()+"]");
				logger.info("Найдено ["+psBCHG.list().size()+"] сторонних начислений");
				if (psBCHG.list().size()!=0)
				{
					
					if (recalcSw.toUpperCase().equals("Y")) 
					{				
							
							String listBCHG= ListToIn(psBCHG.list(), "BILLABLE_CHG_ID");
							
							sb.setLength(0);
							sb.append("SELECT distinct bc.BILLABLE_CHG_ID FROM CI_BSEG_CALC bc \n"); 
							sb.append("join CI_BSEG bs on bs.BSEG_ID = bc.BSEG_ID and bs.BSEG_STAT_FLG in (30, 40, 50) \n");
							sb.append("where bc.BILLABLE_CHG_ID in ( ");
							sb.append(listBCHG);
							sb.append(") AND ROWNUM=1");
							
							String queryBSC = sb.toString();
							
							psBSC = createPreparedStatement(queryBSC);
							if (psBSC.list().size()!=0) 
							{
								logger.info("Пропускаем л/с = [" + acctId + "]");
								
								logError(CmPeni_Batch_MessageRepository.recalculationImpossible(acctId));
								
								throw new Exception(CmPeni_Batch_MessageRepository.recalculationImpossible(acctId).getMessageText());
								
/*								logger.info("Кол-во ошибок "+(nrErrors+1));
								if (nrErrors+1 >= maxErrors)
								{
									addError(CmPeni_Batch_MessageRepository.recalculationImpossible(acctId));
									//throw new Exception(CmPeni_Batch_MessageRepository.recalculationImpossible(acctId).getMessageText());
									return false;
								}
								else
								{
									nrErrors++;
									//throw new Error(CmPeni_Batch_MessageRepository.recalculationImpossible(acctId).getMessageText());
									addWarning(CmPeni_Batch_MessageRepository.recalculationImpossible(acctId));
									 
									return false;
								}*/
							}
							else
							{
								sb.setLength(0);
								sb.append("update CI_BILL_CHG set BILLABLE_CHG_STAT = 20 where BILLABLE_CHG_ID in ( \n");
								sb.append(listBCHG);
								sb.append(") \n");
							
								String queryUpdateBCHG = sb.toString();
								if (logQuery)
									logger.info("queryBCHG = ["+queryUpdateBCHG+"]");
							

								psUpdateBCHG = createPreparedStatement(queryUpdateBCHG);
								
								logger.info("Обновляем статус найденным сторонним начислениям ["+listBCHG+"] на Отменено (20).");
								if (!test)
									logger.info("queryBCHG update counts = ["+psUpdateBCHG.executeUpdate()+"]");

							}
					}
					else 
					{
						logger.info("Пропускаем л/с = [" + acctId + "]");
						return true;
					}
				}
				
				// 4.	Определить наличие активных планов реструктуризации ДЗ для Текущего лицевого счёта. Если есть, то запомнить базовую дату плана реструктуризации ДЗ. Обозначить дату PP_DT.

				psPP.bindId("ACCT_ID", new Account_Id(acctId));
				Date PP_DATE = null;
				if (psPP.list().size()!=0)	
					PP_DATE = psPP.firstRow().getDate("START_DT");
				
				
				// 5.	Определить даты к оплате счетов за обслуживание в расчётном периоде [BEGIN-DT, END-DT] для Текущего лицевого счета
				// 5.1.	Найти все положительные финансовые транзакции с ARS_DT меньше BEGIN-DT и заполненным полем BILL_ID
				// 5.2.	Для найденного счёта за обслуживание найти и сгруппировать все значения DUE_DT + 1, которые попадают в диапазон [BEGIN-DT, END-DT]
				// 5.3.	Запомнить найденные даты в множестве ARRAY-DUE-DT с сортировкой по возрастанию
				
				sb.setLength(0);
				sb.append("select b.DUE_DT + INTERVAL '1' DAY  as DUE_DT, ft.SIBLING_ID, ft.FT_TYPE_FLG from CI_FT ft \n");
				sb.append("JOIN CI_BILL b on b.BILL_ID=ft.BILL_ID and b.ACCT_ID=:ACCT_ID \n");
				sb.append("where ft.ARS_DT<:BEGIN_DT and ft.TOT_AMT>0 and ft.BILL_ID is not null and b.DUE_DT + INTERVAL '1' DAY between :BEGIN_DT and :END_DT \n");
				sb.append("group by b.DUE_DT,ft.SIBLING_ID, ft.FT_TYPE_FLG   \n");
				sb.append("order by b.DUE_DT asc \n");
				String queryFT = sb.toString();
				

				psFT = createPreparedStatement(queryFT);
				psFT.bindId("ACCT_ID", new Account_Id(acctId));
				psFT.bindDate("BEGIN_DT", bgDt);
				psFT.bindDate("END_DT", enDt);
				

				if (logQuery)
					logger.info("psFT = ["+psFT.toString()+"]");
				
				TreeSet<Date> ARRAY_DUE_DT = new TreeSet<Date>();				
				QueryIterator<SQLResultRow> dateIterator = psFT.iterate();
				while(dateIterator.hasNext())
				{
					SQLResultRow row = dateIterator.next();
					Date dueDt = row.getDate("DUE_DT");
					if ((!isBlankOrNull(charTypeAdjDue)) && (row.getString("FT_TYPE_FLG")=="AD"))
					{
					  Adjustment_Id adjID = new Adjustment_Id(row.getString("SIBLING_ID"));
					  if (adjID != null)
				  	  {
					    CharacteristicType_Id charType = new CharacteristicType_Id("CHAR-TYPE-ADJ-DUE");					
					    List adjChar = adjID.getEntity().getSortedCharacteristicsOfType(charType.getEntity());
					    AdjustmentCharacteristic adjC = (AdjustmentCharacteristic) adjChar.get(adjChar.size()-1);
					    
					    logger.info("SIBLING_ID = "+row.getString("SIBLING_ID")+"; CHAR-TYPE-ADJ-DUE = "+adjC.getCharacteristicValue());
					    
						DateFormat df1 = new DateFormat("yyyy-MM-dd");
											
						if (!Date.isValidDateString(adjC.getCharacteristicValue(), df1))
								logInfo(CmPeni_Batch_MessageRepository.incorrectCharTypeAdjDue(row.getString("SIBLING_ID") , adjC.getCharacteristicValue()));			
						try {
							Date NewDate = Date.fromString(adjC.getCharacteristicValue(), df1);
							dueDt = NewDate.addDays(1);
							
						} catch (DateFormatParseException e) {
							e.printStackTrace();
							logInfo(CmPeni_Batch_MessageRepository.incorrectCharTypeAdjDue(row.getString("SIBLING_ID") , adjC.getCharacteristicValue()));	
						}
						
					    
				  	  }
					 
					}
									
					ARRAY_DUE_DT.add(dueDt);
				}
				dateIterator.close();
				
				// 6.	Найти все финансовые транзакции платежей для Текущего лицевого счета:
				//	6.1.	типа PS
				//	6.2.	Исключить финансовые транзакции с РДО, у которого тип РДО не имеет характеристики CHAR-TYPE-CPN-CD и значением Y или y.
				//	6.3.	статус платежа равен Утверждён (50). Идентификатор платежа в поле PARENT_ID
				//	6.4.	ARS_DT принадлежит диапазону [BEGIN-DT, END-DT]
				//	6.5.	Сгруппировать транзакции по полям ARS_DT, SA_ID суммировать поле TOT_AMT

				
				sb.setLength(0);
				sb.append("select ft.ARS_DT, ft.SA_ID, sum(TOT_AMT) as TOT_AMT from CI_FT ft  \n");
				sb.append("join CI_SA sa on sa.SA_ID = ft.SA_ID and sa.ACCT_ID=:ACCT_ID \n");
				sb.append("join CI_SA_TYPE sat on sat.SA_TYPE_CD = sa.SA_TYPE_CD and sa.CIS_DIVISION=sat.CIS_DIVISION \n");
				sb.append("join CI_SA_TYPE_CHAR satc on satc.SA_TYPE_CD = sat.SA_TYPE_CD and trim(satc.CHAR_TYPE_CD) = trim(:CHAR_TYPE_CPN_CD) and  UPPER(trim(satc.CHAR_VAL)) = 'Y' \n");
				sb.append("join CI_PAY pay on pay.PAY_ID = ft.PARENT_ID and pay.PAY_STATUS_FLG = 50 \n");
				sb.append("WHERE ft.FT_TYPE_FLG = 'PS' and ft.ARS_DT between :BEGIN_DT and :END_DT \n");
				sb.append("group by ft.ARS_DT, ft.SA_ID \n");
				
				String queryFT1 = sb.toString();
								
				queryFT1 = queryFT1.replaceAll(":CHAR_TYPE_CPN_CD", "'"+charTypeCpnCd+"'");				
				
				psFT1 = createPreparedStatement(queryFT1);
				psFT1.bindId("ACCT_ID", new Account_Id(acctId));
				psFT1.bindDate("BEGIN_DT", bgDt);
				psFT1.bindDate("END_DT", enDt);
				
				if (logQuery)
					logger.info("psFT1 = ["+psFT1.toString()+"]");
				
				// 7.	Создать множество дат DUE-PAY-DT по алгоритму:
				// 7.1.1.	За основу взять множество ARRAY-DUE-DT
				
				TreeSet<Date> DUE_PAY_DT = new TreeSet<Date>();
				DUE_PAY_DT.addAll((TreeSet<Date>) ARRAY_DUE_DT.clone());
				
				QueryIterator<SQLResultRow> dateIterator1 = psFT1.iterate();
				while(dateIterator1.hasNext())
				{
					SQLResultRow row = dateIterator1.next();
					Date dueDt = row.getDate("ARS_DT");
					DUE_PAY_DT.add(dueDt);
				}
				dateIterator1.close();

				// 7.1.2.	Дополнить значениями ARS_DT ФТ платежей
				// 7.1.3.	дополнить датами BEGIN-DT и END-DT
				// 7.1.4.	сгруппировать и отсортировать даты из множества DUE-PAY-DT


				DUE_PAY_DT.add(bgDt);
				DUE_PAY_DT.add(enDt);
				
		
				logger.info(DUE_PAY_DT.toString());

				String useChar = null;
				if (!isBlankOrNull(charTypeAcctCd))
				{
					
						psBFAcc.bindDate("BEGIN_DT", bgDt);
						psBFAcc.bindId("ACCT_ID", new Account_Id(acctId));
						if (logQuery)
							logger.info(psBFAcc.toString());
						
						if (psBFAcc.list().size()==0) useChar = bfCd;
						else
							useChar = psBFAcc.firstRow().getString("FR"); 
							
				}		
				
				logger.info("Используем фактор расчета из характеристики ["+useChar+"]");
							
							
				// 8.2.	Если не найдено значение фактора на дату равную BEGIN-DT, то создать сообщение «Для фактора расчёта <Код фактора расчёта> не найдено значение на дату <BEGIN-DT>». Пропустить Текущий лицевой счёт			
				sb.setLength(0);
				sb.append("SELECT EFFDT from ( \n");
				sb.append("select VAL, EFFDT from ci_bf_val bfv where bfv.EFFDT<=:BEGIN_DT and TRIM(bfv.BF_CD) = TRIM(:BF_CD) order by EFFDT desc \n");
				sb.append(") TT where ROWNUM=1 \n");
					
				String queryBF = sb.toString();
				
				queryBF = queryBF.replaceAll(":BF_CD", "'"+useChar+"'");
					
				psBF = createPreparedStatement(queryBF);
				psBF.bindDate("BEGIN_DT", bgDt);
				
				if (logQuery)
					logger.info("queryBF = ["+queryBF.toString()+"]");
				
				if (psBF.list().size() == 0)
				{
					
					/*
					logger.info("Кол-во ошибок "+(nrErrors+1));
					if (nrErrors+1 >= maxErrors)
					{
						addError(CmPeni_Batch_MessageRepository.noFactorCalculationOnDate(useChar, beginDt));
//						throw new ThreadAbortedException(CmPeni_Batch_MessageRepository.noFactorCalculationOnDate(useChar, beginDt) );
						return false;
					}
					else
					{
						nrErrors++;
						//throw new Error(CmPeni_Batch_MessageRepository.recalculationImpossible(acctId).getMessageText());
						addWarning(CmPeni_Batch_MessageRepository.noFactorCalculationOnDate(useChar, beginDt));
						return false;
					}
					*/
					logError(CmPeni_Batch_MessageRepository.noFactorCalculationOnDate(useChar, beginDt));
					
					throw new Exception(CmPeni_Batch_MessageRepository.noFactorCalculationOnDate(useChar, beginDt).getMessageText());

				}
				
				
				//8.1.	Определить все значения фактора расчёта на временном отрезке [BEGIN-DT, END-DT], т.е. определить дату начала действия и значение фактора на BEGIN-DT и на все даты начала действия фактора из диапазона [BEGIN-DT, END-DT].
				
				sb.setLength(0);
				sb.append("SELECT DISTINCT EFFDT, VAL from ci_bf_val bfv where bfv.EFFDT>= :BEGIN_DT and bfv.EFFDT<=:END_DT and TRIM(bfv.BF_CD) = TRIM(:BF_CD) \n");
					
				String queryBF1 = sb.toString();
									
				queryBF1 = queryBF1.replaceAll(":BF_CD", "'"+useChar+"'");
									
				psBF1 = createPreparedStatement(queryBF1);
				psBF1.bindDate("BEGIN_DT", bgDt);
				psBF1.bindDate("END_DT", enDt);
				
				
				if (logQuery)
					logger.info("psBF1 = ["+psBF1.toString()+"]");
				
				// 8.3.	Создать новое множество дат PNT-DT, которое состоит из значений:
				// 8.3.1.	За основу взять множество DUE-PAY-DT
				// 8.3.2.	Дополнить датами начала действия значений фактора расчёта
				// 8.3.3.	Выполнить группировку и сортировку по возрастанию множества PNT-DT

				TreeSet<Date> PNT_DT = new TreeSet<Date>();
				PNT_DT.addAll((TreeSet<Date>)DUE_PAY_DT.clone());
				
				QueryIterator<SQLResultRow> dateIterator2 = psBF1.iterate();
				while(dateIterator2.hasNext())
				{
					SQLResultRow row = dateIterator2.next();
					Date effDt = row.getDate("EFFDT");
					PNT_DT.add(effDt);
				}
				dateIterator2.close();
					
				logger.info("PNT_DT = "+PNT_DT.toString());
				
				Iterator<Date> itr = PNT_DT.iterator();
				
				ArrayList<TemplateRow> template = new ArrayList<TemplateRow>();

				sb.setLength(0);
				sb.append("SELECT VAL from ( \n");
				sb.append("select VAL, EFFDT from ci_bf_val bfv where bfv.EFFDT<=:BEGIN_DT and TRIM(bfv.BF_CD) = TRIM(:BF_CD) order by EFFDT desc \n");
				sb.append(") TT where ROWNUM=1 \n");
					
				String queryTemplate = sb.toString();
									
				queryTemplate = queryTemplate.replaceAll(":BF_CD", "'"+useChar+"'");

				psTemplate = createPreparedStatement(queryTemplate);
				psTemplate.setAutoclose(false);

				// 9.	Организовать матрицу данных (временная таблица). Далее Шаблон
				// 9.1.	Поля
				// 9.1.1.	Date: Заполнить значениями из PNT-DT.
				// 9.1.2.	Base: null
				// 9.1.3.	Rate: null
				// 9.1.4.	Sa_id: null

				while (itr.hasNext()) {
					Date dt = itr.next();
				
					psTemplate.bindDate("BEGIN_DT", dt);
					
					if (logQuery)
						logger.info("psTemplate = ["+psTemplate.toString()+"]");
					
					
					// 9.2.	Заполнить поле Rate для всех строк Шаблона. Определить на каждую дату из Шаблона значение фактора расчёта, найденного в п.8
					template.add(new TemplateRow(dt, null, psTemplate.firstRow().getString("VAL"), null));
				}
				if (template.size()==1)
					template.add(
							new TemplateRow(template.get(0).date, template.get(0).base, template.get(0).rate, template.get(0).saId));
				
				logger.info("Template = "+template.toString());
				logger.info("DUE_PAY_DT = "+DUE_PAY_DT.toString());
				
				// 10.	 (ЦИКЛ по РДО) Выбрать РДО лицевого счёта:
				// 10.1.	 в статусе Активный (20), Ожидает завершения (30), Остановлен (40) и Повторно Активирован (50).
				// 10.2.	Тип РДО используется для расчёта Пени: существует характеристика типа РДО CHAR-TYPE-CPN-CD со значением Y или y

				sb.setLength(0);
				sb.append("select distinct sa.SA_ID from CI_SA sa \n");
				sb.append("join CI_SA_TYPE sat on sat.SA_TYPE_CD = sa.SA_TYPE_CD and sa.CIS_DIVISION = sat.CIS_DIVISION \n");
				sb.append("join CI_SA_TYPE_CHAR satc on satc.SA_TYPE_CD = sa.SA_TYPE_CD and trim(satc.CHAR_TYPE_CD) =trim(:CHAR_TYPE_CPN_CD) and UPPER(trim(satc.CHAR_VAL)) = 'Y' \n");
				sb.append("WHERE sa.ACCT_ID=:ACCT_ID and sa.SA_STATUS_FLG in (20, 30, 40, 50) \n");
					
				String querySA = sb.toString();
									
				querySA = querySA.replaceAll(":CHAR_TYPE_CPN_CD", "'"+charTypeCpnCd+"'");				
				
				psSA = createPreparedStatement(querySA);
				psSA.bindId("ACCT_ID", new Account_Id(acctId));
				
				if (logQuery)
					logger.info("querySA = ["+querySA.toString()+"]");
				
				
				QueryIterator<SQLResultRow> saIterator = psSA.iterate();
				
				ArrayList<ArrayList<TemplateRow>> templateAll = new ArrayList<ArrayList<TemplateRow>>();
				
				Date[] aDUE_PAY_DT = null;
				
				if (DUE_PAY_DT.size() ==1)
				{
					aDUE_PAY_DT = DUE_PAY_DT.toArray(new Date[2]);
					aDUE_PAY_DT[1] = new Date(aDUE_PAY_DT[0].getYear(),aDUE_PAY_DT[0].getMonth(),aDUE_PAY_DT[0].getDay());
				}
				else
					aDUE_PAY_DT = DUE_PAY_DT.toArray(new Date[0]);
			
				
				logger.info("aDUE_PAY_DT = "+aDUE_PAY_DT.toString());
				
				
				
				sb.setLength(0);
				sb.append("select nvl(sum(nvl(TT.TOT_AMT,0.0)),0.0) as TOT_AMT from  \n");
				sb.append("(select  ft.FT_ID, ft.ARS_DT, ft.TOT_AMT,  \n");
				sb.append("case ft.FT_TYPE_FLG \n");
				sb.append(" when 'BX' then (select ft1.BILL_ID from CI_FT ft1 where ft1.SIBLING_ID=ft.SIBLING_ID and ft1.FT_TYPE_FLG='BS') \n");
				sb.append(" when 'BS' then ft.BILL_ID \n");
				sb.append("end as BILL_ID \n");
				sb.append("from CI_FT ft \n");
				sb.append("WHERE ft.SA_ID = :SA_ID and ft.FT_TYPE_FLG in ('BS','BX') and ft.BILL_ID is not null \n");
				sb.append(") TT \n");
				sb.append("JOIN CI_BILL b on b.BILL_ID=TT.BILL_ID and b.DUE_DT <= :DATE_I_1  \n");
				sb.append("and TT.ARS_DT>=nvl(:PP_DT, TT.ARS_DT) \n");
				
				String querySEG = sb.toString();
					
				psSEG = createPreparedStatement(querySEG);
				psSEG.setAutoclose(false);

				
				sb.setLength(0);
				sb.append("select nvl(sum(nvl(TT.TOT_AMT,0.0)),0.0) as TOT_AMT from  \n");
				sb.append("(select  ft.FT_ID, ft.ARS_DT, ft.TOT_AMT,  \n");
				sb.append("case ft.FT_TYPE_FLG \n");
				sb.append(" when 'AX' then (select ft1.BILL_ID from CI_FT ft1 where ft1.SIBLING_ID=ft.SIBLING_ID and ft1.FT_TYPE_FLG='AD') \n");
				sb.append(" when 'AD' then ft.BILL_ID \n");
				sb.append("end as BILL_ID \n");
				sb.append("from CI_FT ft \n");
				sb.append("WHERE ft.SA_ID = :SA_ID and ft.FT_TYPE_FLG in ('AX','AD') and ft.BILL_ID is not null \n");
				sb.append(") TT \n");
				sb.append("JOIN CI_BILL b on b.BILL_ID=TT.BILL_ID and b.DUE_DT <= :DATE_I_1  \n");
				sb.append("and TT.ARS_DT>=nvl(:PP_DT, TT.ARS_DT) \n");
				
				String queryCorr = sb.toString();
					
				psCorr = createPreparedStatement(queryCorr);
				psCorr.setAutoclose(false);
				
				sb.setLength(0);
				sb.append("select nvl(sum(nvl(TOT_AMT,0.0)),0.0) as TOT_AMT from CI_FT ft   \n");
				sb.append("WHERE ft.SA_ID = :SA_ID and ft.FT_TYPE_FLG in ('PS','PX')   \n");
				sb.append("and ft.ARS_DT<=:DATE_I_1 \n");
				sb.append("and ft.ARS_DT>=nvl(:PP_DT, ft.ARS_DT) \n");
				
				String queryPay = sb.toString();
					
				psPay = createPreparedStatement(queryPay);
				psPay.setAutoclose(false);

				sb.setLength(0);
				sb.append("select distinct sa.SA_ID from CI_SA sa \n");
				sb.append("join CI_SA_TYPE sat on sat.SA_TYPE_CD=sa.SA_TYPE_CD and sa.CIS_DIVISION=sat.CIS_DIVISION \n");
				sb.append("join CI_SA_TYPE_CHAR satc on satc.SA_TYPE_CD = sat.SA_TYPE_CD and trim(satc.CHAR_TYPE_CD) =trim(:CHAR_TYPE_SAT_CD) and UPPER(trim(satc.CHAR_VAL)) = UPPER(trim(:CHAR_VAL_SAT))  \n");
				sb.append("WHERE sa.ACCT_ID=:ACCT_ID and sa.SA_STATUS_FLG in (20, 30, 40, 50) \n");
					
				String querySAPeni = sb.toString();
									
				querySAPeni = querySAPeni.replaceAll(":CHAR_TYPE_SAT_CD", "'"+charTypeSatCd+"'");				
				querySAPeni = querySAPeni.replaceAll(":CHAR_VAL_SAT", "'"+charValSat+"'");
				
				psSAPeni = createPreparedStatement(querySAPeni);
				
				if (logQuery)
					logger.info("psSAPeni = "+psSAPeni.toString());
				
				psSAPeni.setAutoclose(false);
				
				while(saIterator.hasNext())
				{
					// 10.3.1.	Сделать копию Шаблона
					ArrayList<TemplateRow> templateCopy = new ArrayList<TemplateRow>();
					
					for (TemplateRow tr : template)
					{
						templateCopy.add(new TemplateRow(tr.date, tr.base, tr.rate, tr.saId));
					}
	
									
					logger.info("templateCopy (var1) = "+templateCopy.toString()+"");
					
					SQLResultRow row = saIterator.next();
					String saId = row.getString("SA_ID");
					logger.info("SA_ID = ["+saId+"]");
					
					// 10.3.2.	 обновить во всех строках Sa_id на значение текущего РДО из цикла
					for (TemplateRow tr : templateCopy)
					{
						tr.saId =  saId;
					}
					
					logger.info("templateCopy (var2) = "+templateCopy.toString()+"");
					

					// 10.4.	Для текущего РДО запустить цикл по множеству дат из DUE-PAY-DT[i], начиная со второго значения (i=2), где i = 2, 3… При этом  DUE-PAY-DT[1] = BEGIN_DT				
					for (int i=1; i<=aDUE_PAY_DT.length-1; i++)
					{


						// 10.5.	Рассчитать Базу пени на интервале [DUE-PAY-DT [i-1]; DUE-PAY-DT [i]-1]
						// 10.5.1.1.	Суммировать начисления и отмены сегментов счетов:
						// 10.5.1.1.1.	Тип финансовой транзакции BS, BX
						// 10.5.1.1.2.	Поле BILL_ID заполнено.Если тип финансовой транзакции BX, то BILL_ID переопределить значением BILL_ID финансовой транзакции с тем же SIBLING_ID и типом финансовой транзакции BS.
						// 10.5.1.1.3.	Значение поля DUE_DT (Дата к оплате) счёта за обслуживание, связанного с финансовой транзакцией, меньше DUE-PAY-DT[i-1]
						// 10.5.1.1.4.	ARS_DT больше или равна PP-DT, если PP-DT найдена

						logger.info("aDUE_PAY_DT[i-1] = "+aDUE_PAY_DT[i-1].toString());
						
						psSEG.bindDate("DATE_I_1", aDUE_PAY_DT[i-1]);
						psSEG.bindDate("PP_DT", PP_DATE);
						psSEG.bindId("SA_ID", new ServiceAgreement_Id(saId));
						
						if (logQuery)
							logger.info("psSEG = "+psSEG.toString());
						
						BigDecimal sumSEG = new BigDecimal(0);
						
						if (psSEG.list().size()>0)
							sumSEG = psSEG.firstRow().getBigDecimal("TOT_AMT");
						logger.info("sumSEG = "+sumSEG);
						
						// 10.5.1.2.	Суммировать начисления корректировок
						// 10.5.1.2.1.	Тип финансовой транзакции AD, AX
						// 10.5.1.2.2.	Поле BILL_ID заполнено. Если тип финансовой транзакции AX, то BILL_ID переопределить значением BILL_ID финансовой транзакции с тем же SIBLING_ID и типом финансовой транзакции AD.
						// 10.5.1.2.3.	Значение поля DUE_DT (Дата к оплате), счёта за обслуживание, связанного с финансовой транзакцией меньше DUE-PAY-DT[i]
						// 10.5.1.2.4.	ARS_DT больше или равна PP-DT, если PP-DT найдена

						psCorr.bindDate("DATE_I_1", aDUE_PAY_DT[i-1]);
						psCorr.bindDate("PP_DT", PP_DATE);
						psCorr.bindId("SA_ID", new ServiceAgreement_Id(saId));
						if (logQuery)
							logger.info("psCorr = "+psCorr.toString());
						
						BigDecimal sumCorr = new BigDecimal(0);
						if (psCorr.list().size()>0)
							sumCorr = psCorr.firstRow().getBigDecimal("TOT_AMT");
						logger.info("sumCorr = "+sumCorr);
						
						// 10.5.2.	Суммировать оплаты
						// 10.5.2.1.	Тип финансовой транзакции PS, PX
						// 10.5.2.2.	ARS_DT меньше или равен DUE-PAY-DT[i]
						// 10.5.2.3.	ARS_DT больше или равна PP-DT, если PP-DT найдена

						psPay.bindDate("DATE_I_1", aDUE_PAY_DT[i-1]);
						psPay.bindDate("PP_DT", PP_DATE);
						psPay.bindId("SA_ID", new ServiceAgreement_Id(saId));
						if (logQuery)
							logger.info("psPay = "+psPay.toString());
						
						BigDecimal sumPay = new BigDecimal(0);
						if (psPay.list().size()>0)
							 sumPay = psPay.firstRow().getBigDecimal("TOT_AMT");
						logger.info("sumPay = "+sumPay);
						
						// 10.5.3.	База для DUE-PAY-DT [i-1] равна сумме итогов ФТ сегментов, корректировок и платежей. Обозначим сумма как BASE[i-1]
						BigDecimal BASE_I_1 = sumSEG.add(sumCorr).add(sumPay);
//						BigDecimal BI1 = new BigDecimal(sumSEG + sumCorr + sumPay);
//						logger.info(BASE_I_1);

						
						// 10.5.4.	Обновить строки Экземпляра Шаблона. Поле Base равно значению BASE[i-1] при условии Date принадлежит периоду [DUE-PAY-DT [i-1]; DUE-PAY-DT [i]-1]
						for (TemplateRow tr : templateCopy)
						{
							if (!aDUE_PAY_DT[i-1].equals(aDUE_PAY_DT[i]))
							{
								if ((tr.date.isSameOrAfter(aDUE_PAY_DT[i-1])) && (tr.date.isSameOrBefore(aDUE_PAY_DT[i].addDays(-1))))
									tr.base = BASE_I_1;
							}
							else
							{
								if (((tr.date.isSameOrAfter(aDUE_PAY_DT[i-1])) && (tr.date.isSameOrBefore(aDUE_PAY_DT[i]))) && (templateCopy.get(templateCopy.size()-1)!=tr))
									tr.base = BASE_I_1;
								
							}
						}
						logger.info("templateCopy (var3) = "+templateCopy.toString()+"");
											
						
					}
					
					// 10.6.	Исключить строки Экземпляра Шаблона, где Base <= 0
					
					Iterator<TemplateRow> el = templateCopy.iterator();
					while (el.hasNext()) {
						TemplateRow s = el.next(); // must be called before you can call i.remove()
						if (((isNull(s.base) || (s.base.compareTo(BigDecimal.ZERO)<=0)) && (el.hasNext()))) el.remove(); //!!!!!! <=
					}
					
					logger.info("templateCopy (final) = "+templateCopy.toString());
					
					if (templateCopy.size()>1)
						templateAll.add(templateCopy);
					
					
					logger.info("template all = " + templateAll.toString());
				

				}
				saIterator.close();
				
				// Если строк в Едином экземпляре Шаблона не осталось, то  перейти к следующему лицевому счету
				if (templateAll.size() == 0) 
				{	
					logger.info("строк в Едином экземпляре Шаблона не осталось, то  перейти к следующему лицевому счету");
					return true;
				}
				
				// 12.1.	Найти РДО Пени
				// 12.1.1.	Тип РДО содержит тип характеристики типа CHAR-TYPE-SAT-CD и значением равным CHAR-VAL-SAT.
				// 12.1.2.	Статус РДО: Активный (20), Ожидает завершения (30), Остановлен (40), Повторно активирован (50)



				psSAPeni.bindId("ACCT_ID", new Account_Id(acctId));	
				
				ServiceAgreement_Id saId = null;
				if (psSAPeni.list().size()>0)
				{
					saId =  new ServiceAgreement_Id(psSAPeni.firstRow().getString("SA_ID"));
					logger.info("Найден РДО Пени = ["+saId.getIdValue()+"]");
				}
				else
				{  
					// 12.2.	Если не найден
					// 12.2.1.	Создать РДО типа: найти тип РДО с характеристикой типа CHAR-TYPE-SAT-CD и значением CHAR-VAL-SAT
					// 12.2.1.1.	Дата начала действия РДО: BEGIN-DT
					// 12.2.2.	Активировать РДО

					ServiceAgreement_DTO saDTO = (ServiceAgreement_DTO) createDTO(ServiceAgreement.class);
					Account_Id account = new Account_Id(acctId); 
					saDTO.setAccountId(account);
					
					String saType="";
					
					sb.setLength(0);
					sb.append("select st.SA_TYPE_CD from ci_sa_type st \n");
					sb.append("join ci_sa_type_char sac on sac.SA_TYPE_CD = st.SA_TYPE_CD and \n");
					sb.append("trim(sac.CHAR_TYPE_CD) =trim(:CHAR_TYPE_SAT_CD) and trim(sac.CHAR_VAL)=trim(:CHAR_VAL_SAT) \n");
					
					String querySATY = sb.toString();
					
					querySATY = querySATY.replaceAll(":CHAR_TYPE_SAT_CD", "'"+charTypeSatCd+"'");
					querySATY = querySATY.replaceAll(":CHAR_VAL_SAT", "'"+charValSat+"'");
					
					psSATY = createPreparedStatement(querySATY);
					if (logQuery)
						logger.info("psSATY = ["+psSATY.toString()+"]");
					
					saType = psSATY.firstRow().getString("SA_TYPE_CD");

/*					CisDivision_Id cdId = new CisDivision_Id("MG"); */
					ServiceAgreementType_Id saTypeId = new ServiceAgreementType_Id(account.getEntity().getDivisionId(), saType);
//					saDTO.setVersion(1);
					saDTO.setCustomerRead(CustomerReadLookup.constants.NO);
					
					saDTO.setServiceAgreementTypeId(saTypeId);
					saDTO.setStartDate(bgDt);				
				
					saDTO.setStatus(ServiceAgreementStatusLookup.constants.ACTIVE);
					

					logger.info("Будет создан РДО для л/с ["+acctId+"] с типом ["+saType+"] (с характеристикой типа ["+charTypeSatCd+"]");

					if (!test)
					{						
						ServiceAgreement sa = saDTO.newEntity();
						saId =  sa.getId();
					}
					else saId = new ServiceAgreement_Id("7780641744");
					
					logger.info("Создан РДО для л/с ["+saId.getEntity().getAccount().getId().getIdValue()+"] с типом ["+
							saId.getEntity().getServiceAgreementType().getId().getSaType()
							+"] (с характеристикой типа ["+charTypeSatCd+"] значением [" + charValSat + "]) = ["+saId.getIdValue()+"]");
					
				}
				
				// Табл. 53 Таблица CI_BILL_CHG
				BillableCharge_DTO bcDTO = (BillableCharge_DTO) createDTO(BillableCharge.class);
				bcDTO.setServiceAgreementId(saId);
				bcDTO.setStartDate(bgDt);
				bcDTO.setEndDate(enDt);
				bcDTO.setDescriptionOnBill("Начисление Пени с "+beginDt+" по "+endDt);
				bcDTO.setBillableChargeStatus(BillableChargeStatusLookup.constants.BILLABLE);
				bcDTO.setVersion(1);
				bcDTO.setIlmDate(getSystemDateTime().toDate());
				bcDTO.setIsEligibleForArchiving(Bool.FALSE);
				BillableCharge bc = null; 
				
				logger.info(String.format("Будет создано строка в таблице CI_BILL_CHG: \n "
						+ "BILLABLE_CHG_ID:	Сгенерировано средствами CC&B \n"
						+ "SA_ID:	%s \n"
						+ "START_DT: %s \n"
						+ "END_DT:  %s \n"
						+ "DESCR_ON_BILL: %s \n"
						+ "BILLABLE_CHG_STAT: %s \n", bcDTO.getServiceAgreementId(), bcDTO.getStartDate().toString(), bcDTO.getEndDate().toString(), bcDTO.getDescriptionOnBill(),
						bcDTO.getBillableChargeStatus().getLookupValue().getId().getFieldValue()));
				
				if (!test) 
				{
					bc = bcDTO.newEntity();
				}
				else
				{
					BillableCharge_Id bcc = new BillableCharge_Id("023212265063"); //048800329023
					bc = bcc.getEntity();
				}
				logger.info("BILLABLE_CHG_ID = "+bc.getId().getIdValue());


				// Для каждой строки Единого экземпляра шаблона:
				
				BigInteger j=BigInteger.ZERO;
				
				for (ArrayList<TemplateRow> trL : templateAll)
				{
					
					BigInteger i=BigInteger.ZERO;
					
					for (TemplateRow tr : trL)

				{

					
					if (tr.base!=null) // значит мы находимся на последнем элементе

					{
						TemplateRow tr_1 = trL.get(i.intValue()+1);
						
						// Табл. 54 Таблица CI_B_CHG_LINE
						i = i.add(BigInteger.ONE);
						j = j.add(BigInteger.ONE);
						
						
						BillableChargeLine_DTO bcLineDTO = (BillableChargeLine_DTO) createDTO(BillableChargeLine.class);
						BillableChargeLine_Id bclId = new BillableChargeLine_Id(bc.getId(), j);
						
						bcLineDTO.setId(bclId);
						long dif = 0;
						if ((i.intValue()<trL.size()-1))
						{
						bcLineDTO.setDescriptionOnBill("Начисление Пени с "+tr.date+" по "+ 
								tr_1.date.addDays(-1));
						dif = tr_1.date.difference(tr.date).getTotalDays();
						}
						else
						{
							bcLineDTO.setDescriptionOnBill("Начисление Пени с "+tr.date+" по "+ 
									tr_1.date);
							dif = tr_1.date.difference(tr.date).getTotalDays()+1;
						}
						
						//double bg = tr.base.doubleValue()*Double.valueOf(tr.rate)*(1/100.0) *dif /365.0;
						//bg = Math.round(bg*100)/100.00;
						
						BigDecimal bG = tr.base.multiply(new BigDecimal(tr.rate)).divide(new BigDecimal("100")).multiply(new BigDecimal(dif)).divide(new BigDecimal("365")).setScale(2, BigDecimal.ROUND_HALF_UP);
						//if (bg!=bG.doubleValue()) logger.info("!!!!!!!!!! Eroare !!!!!!!!!! ["+bg+" --> "+bG+"]");
						//if (!String.valueOf(bg).equals(bG.toString())) logger.info("!!!!!!!!!! Eroare !!!!!!!!!! ["+bg+" --> "+bG+"]");
						
						bcLineDTO.setChargeAmount(new Money (bG.toString()));  //String.valueOf(bg)
						bcLineDTO.setCurrencyId(saId.getEntity().getCurrencyId());
						bcLineDTO.setShouldShowOnBill(Bool.TRUE);
						bcLineDTO.setShouldAppearInSummary(Bool.FALSE);
						bcLineDTO.setDistributionCodeId(saId.getEntity().getServiceAgreementType().getDistributionCode().getId());
						bcLineDTO.setVersion(1);
						bcLineDTO.setIsMemoOnly(null);
						
						
						logger.info(String.format("\nБудет создана строка "+j.intValue()+" в таблице CI_B_CHG_LINE: \n "
								+ "DESCR_ON_BILL:	%s \n"
								+ "CHARGE_AMT:	%s \n"
								+ "CURRENCY_CD: %s \n"
								+ "SHOW_ON_BILL_SW:  %s \n"
								+ "APP_IN_SUMM_SW: %s \n"
								+ "DST_ID: %s \n", bcLineDTO.getDescriptionOnBill(), bcLineDTO.getChargeAmount().getAmount().toString(), bcLineDTO.getCurrencyId().getIdValue() , 
								bcLineDTO.getShouldShowOnBill().toString(), bcLineDTO.getShouldAppearInSummary().toString(),
								bcLineDTO.getDistributionCodeId().getIdValue()));
						
						if (!test) 
						{
							BillableChargeLine bcl = bcLineDTO.newEntity();
							logger.info("BILLABLE_CHG_ID = "+bcl.getId().getBillableChargeId().getIdValue()+" LINESQ = "+bcl.getId().getLineSequence());
						}
						
						
						StringBuilder chars = new StringBuilder();
						chars.append("\nХарактеристики строки (CI_B_LN_CHAR): \n");
								
						// Табл. 55 Таблица CI_B_LN_CHAR. Базовая сумма
						BillableChargeLineCharacteristic_DTO bcLineCharDTO = (BillableChargeLineCharacteristic_DTO) createDTO(BillableChargeLineCharacteristic.class);
						
//						String charTypeClAmtCD = getParameters().getCHAR_TYPE_CL_AMT_CD();

						CharacteristicType_Id charType = null;
						
						if (!isBlankOrNull(charTypeClAmtCd))
						{
							charType = new CharacteristicType_Id(charTypeClAmtCd); 
						}
											
						BillableChargeLineCharacteristic_Id bclcId = new BillableChargeLineCharacteristic_Id(bclId, charType);
						
						bcLineCharDTO.setId(bclcId);
						bcLineCharDTO.setCharacteristicValue("");
						bcLineCharDTO.setAdhocCharacteristicValue(tr.base.toString());
						bcLineCharDTO.setCharacteristicValueForeignKey1("");		
						bcLineCharDTO.setCharacteristicValueFK2("");
						bcLineCharDTO.setCharacteristicValueFK3("");
						bcLineCharDTO.setCharacteristicValueFk4("");
						bcLineCharDTO.setCharacteristicValueFK5("");
						bcLineCharDTO.setVersion(1);
						
						chars.append(String.format("PN-BASE: %s \n", bcLineCharDTO.getAdhocCharacteristicValue())); 
						
						if (!test) 
						{
							//BillableChargeLineCharacteristic bclc = 
							bcLineCharDTO.newEntity();
							
						}
						

						// Табл. 56 Таблица CI_B_LN_CHAR. Начальная дата
						BillableChargeLineCharacteristic_DTO bcLineCharBeginDTO = (BillableChargeLineCharacteristic_DTO) createDTO(BillableChargeLineCharacteristic.class);
						
//						String charTypeClSdCd = getParameters().getCHAR_TYPE_CL_SD_CD();

						CharacteristicType_Id charTypeBegin = null;
						
						if (!isBlankOrNull(charTypeClSdCd))
						{
							charTypeBegin = new CharacteristicType_Id(charTypeClSdCd); 
						}
											
						BillableChargeLineCharacteristic_Id bclcBeginId = new BillableChargeLineCharacteristic_Id(bclId, charTypeBegin);
						
						bcLineCharBeginDTO.setId(bclcBeginId);
						bcLineCharBeginDTO.setCharacteristicValue("");
						DateFormat dfYYYYMMDD = new DateFormat("yyyy-MM-dd");
						
						bcLineCharBeginDTO.setAdhocCharacteristicValue(tr.date.toString(dfYYYYMMDD));
						bcLineCharBeginDTO.setCharacteristicValueForeignKey1("");		
						bcLineCharBeginDTO.setCharacteristicValueFK2("");
						bcLineCharBeginDTO.setCharacteristicValueFK3("");
						bcLineCharBeginDTO.setCharacteristicValueFk4("");
						bcLineCharBeginDTO.setCharacteristicValueFK5("");
						bcLineCharBeginDTO.setVersion(1);
						
						chars.append(String.format("PN-S-DT: %s \n", bcLineCharBeginDTO.getAdhocCharacteristicValue()));
																	
						if (!test) 
						{
							//BillableChargeLineCharacteristic bclcBegin = 
							bcLineCharBeginDTO.newEntity();
						}
						
						
						// Табл. 57 Таблица CI_B_LN_CHAR. Конечная дата
						BillableChargeLineCharacteristic_DTO bcLineCharEndDTO = (BillableChargeLineCharacteristic_DTO) createDTO(BillableChargeLineCharacteristic.class);
						
//						String charTypeClEdCd = getParameters().getCHAR_TYPE_CL_ED_CD();

						CharacteristicType_Id charTypeEnd = null;
						
						if (!isBlankOrNull(charTypeClEdCd))
						{
							charTypeEnd = new CharacteristicType_Id(charTypeClEdCd); 
						}
											
						BillableChargeLineCharacteristic_Id bclcEndId = new BillableChargeLineCharacteristic_Id(bclId, charTypeEnd);
						
						bcLineCharEndDTO.setId(bclcEndId);
						bcLineCharEndDTO.setCharacteristicValue("");
					
						if (i.intValue()<trL.size()-1)
							bcLineCharEndDTO.setAdhocCharacteristicValue( tr_1.date.addDays(-1).toString(dfYYYYMMDD));
						else 
							bcLineCharEndDTO.setAdhocCharacteristicValue( tr_1.date.toString(dfYYYYMMDD));
						bcLineCharEndDTO.setCharacteristicValueForeignKey1("");		
						bcLineCharEndDTO.setCharacteristicValueFK2("");
						bcLineCharEndDTO.setCharacteristicValueFK3("");
						bcLineCharEndDTO.setCharacteristicValueFk4("");
						bcLineCharEndDTO.setCharacteristicValueFK5("");
						bcLineCharEndDTO.setVersion(1);
						
						chars.append(String.format("PN-E-DT: %s \n", bcLineCharEndDTO.getAdhocCharacteristicValue()));
						
						if (!test) 
						{
							// BillableChargeLineCharacteristic bclcEnd = 
							bcLineCharEndDTO.newEntity();
						}

						//Таблица CI_B_LN_CHAR. Ставка
						BillableChargeLineCharacteristic_DTO bcLineCharRateDTO = (BillableChargeLineCharacteristic_DTO) createDTO(BillableChargeLineCharacteristic.class);
						
//						String charTypeClRtCd = getParameters().getCHAR_TYPE_CL_RT_CD();

						CharacteristicType_Id charTypeRate = null;
						
						if (!isBlankOrNull(charTypeClRtCd))
						{
							charTypeRate = new CharacteristicType_Id(charTypeClRtCd); 
						}
											
						BillableChargeLineCharacteristic_Id bclcRateId = new BillableChargeLineCharacteristic_Id(bclId, charTypeRate);
						
						bcLineCharRateDTO.setId(bclcRateId);
						bcLineCharRateDTO.setCharacteristicValue("");
					
						bcLineCharRateDTO.setAdhocCharacteristicValue( tr.rate);
						bcLineCharRateDTO.setCharacteristicValueForeignKey1("");		
						bcLineCharRateDTO.setCharacteristicValueFK2("");
						bcLineCharRateDTO.setCharacteristicValueFK3("");
						bcLineCharRateDTO.setCharacteristicValueFk4("");
						bcLineCharRateDTO.setCharacteristicValueFK5("");
						bcLineCharRateDTO.setVersion(1);
						
						chars.append(String.format("PN-RATE: %s \n", bcLineCharRateDTO.getAdhocCharacteristicValue()));
						
						if (!test) 
						{
							//BillableChargeLineCharacteristic bclcRate = 
							bcLineCharRateDTO.newEntity();
						}						

						
						// Таблица CI_B_LN_CHAR. РДО Задолженности
						BillableChargeLineCharacteristic_DTO bcLineCharSaDTO = (BillableChargeLineCharacteristic_DTO) createDTO(BillableChargeLineCharacteristic.class);
						
//						String charTypeClSaCd = getParameters().getCHAR_TYPE_CL_SA_CD();

						CharacteristicType_Id charTypeSa = null;
						
						if (!isBlankOrNull(charTypeClSaCd))
						{
							charTypeSa = new CharacteristicType_Id(charTypeClSaCd); 
						}
											
						BillableChargeLineCharacteristic_Id bclcSaId = new BillableChargeLineCharacteristic_Id(bclId, charTypeSa);
						
						bcLineCharSaDTO.setId(bclcSaId);
						bcLineCharSaDTO.setCharacteristicValue("");
					
						bcLineCharSaDTO.setAdhocCharacteristicValue("");
						bcLineCharSaDTO.setCharacteristicValueForeignKey1(tr.saId);		
						bcLineCharSaDTO.setCharacteristicValueFK2("");
						bcLineCharSaDTO.setCharacteristicValueFK3("");
						bcLineCharSaDTO.setCharacteristicValueFk4("");
						bcLineCharSaDTO.setCharacteristicValueFK5("");
						bcLineCharSaDTO.setVersion(1);
						
						chars.append(String.format("PN-SA: %s \n", bcLineCharSaDTO.getCharacteristicValueForeignKey1()));
						logger.info(chars.toString());
																	
						if (!test) 
						{
							// BillableChargeLineCharacteristic bclcSa = 
							bcLineCharSaDTO.newEntity();
						}		
						
					}
				}
					
				}
				
				
				
			}
			catch (Exception E)
			{
				success = false;
				String s = (E.getMessage()==null ? E.toString() : E.getMessage() );
				
				/*
				 char[] buf = new char[1024];
				 
				
				
				
				int length = s.length();
				char[] oldChars = (length < 1024) ? buf : new char[length];
				s.getChars(0, length, oldChars, 0);
				int newLen = 0;
				for (int j = 0; j < length; j++) {
				    char ch = oldChars[j];
				    if (ch > ' ') {
				        oldChars[newLen] = ch;
				        newLen++;
				    }
				}
				if (newLen != length)
				    s = new String(oldChars, 0, newLen);
				*/
				
				ServerMessage mes = CmPeni_Batch_MessageRepository.errorAccountId(acctId, s);

				logError(mes);				
				logger.error(mes.getMessageText());

				addError(mes, MessageSeverityLookup.constants.INFORMATIONAL);
				
				return false;
			}
			finally
			{
				if (psBCHG!=null)
					psBCHG.close();
				if (psUpdateBCHG!=null)
					psUpdateBCHG.close();
				if (psBSC!=null)
					psBSC.close();
				if (psPP!=null)
					psPP.close();
				if (psFT!=null)
					psFT.close();
				if (psFT1!=null)
					psFT1.close();					
				if (psTemplate!=null)
					psTemplate.close();	
				if (psBF!=null)
					psBF.close();
				if (psBF1!=null)
					psBF1.close();
				
				if (psSA!=null)
					psSA.close();					
				
				if (psSEG!=null)
					psSEG.close();					
				if (psCorr!=null)
					psCorr.close();					
				if (psPay!=null)
					psPay.close();					
				if (psSAPeni!=null)
					psSAPeni.close();

				if (psBFAcc!=null)
					psBFAcc.close();

				
			if (psSATY!=null)
				psSATY.close();	
			
			if (success)
	        {
	          this.okNumb += 1;
	          saveChanges();
	        }
	        else
	        {
	          rollbackToSavepoint(savePointName);
	          this.errNumb += 1;
	          if (this.errNumb >= maxErrors) {
					ServerMessage mes = com.splwg.base.domain.batch.batchRun.MessageRepository.maxWorkSumbissionAttemptsReached(maxErrors); 
					logError(mes);
					logger.error( mes );
					
					throw new RunAbortedException(mes );
					
	          }
	          
	        }
				
			}
		
			
			return true;
		}

	}
	
	static String ListToIn(List<SQLResultRow> list, String field) {
		String s = "";
		for (SQLResultRow param : list) {
			if (s.isEmpty())
				s = s + "\'" + param.getString(field) + "\'";
			else
				s = s + ", \'" + param.getString(field) + "\'";
		}

		return s;

	}


}
