package com.splwg.cm.domain.financial;

import java.math.BigInteger;

import com.splwg.base.domain.common.characteristicType.CharacteristicType;
import com.splwg.base.domain.common.featureConfiguration.FeatureConfiguration;
import com.splwg.base.domain.common.featureConfiguration.FeatureConfigurationOption;
import com.splwg.base.domain.common.message.MessageParameters;
import com.splwg.ccb.domain.todo.toDoCase.entity.ToDoCase;
import com.splwg.cm.domain.common.CmApiFeatureConfigurationWrapper;
import com.splwg.shared.common.ServerMessage;

/**
 * @messageRepository
 */

public class CmPeni_Batch_MessageRepository extends CmPeni_Batch_Messages {

	private static CmPeni_Batch_MessageRepository instance;

	private static CmPeni_Batch_MessageRepository getInstance() {
		if (instance == null) {
			instance = new CmPeni_Batch_MessageRepository();
		}
		return instance;
	}


	/**
	 * @message INCORRECT_RECALC_SW
	 * @description ENG "Значение параметра RECALC-SW (%1) не корректное" "Значение параметра RECALC-SW (%1) не корректное"
	 * @description RUS "Значение параметра RECALC-SW (%1) не корректное" "Значение параметра RECALC-SW (%1) не корректное"
	 * @description RUM "Значение параметра RECALC-SW (%1) не корректное" "Значение параметра RECALC-SW (%1) не корректное"
	 */
	public static ServerMessage incorrectRecalcSw(String recalcSw) {
		MessageParameters p = new MessageParameters();
		p.addRawString(recalcSw);
		return getInstance().getMessage(INCORRECT_RECALC_SW, p);
	}


	/**
	 * @message INCORRECT_BEGIN_DT
	 * @description ENG "Значение параметра BEGIN-DT (%1) не является датой" "Значение параметра BEGIN-DT не является датой"
	 * @description RUS "Значение параметра BEGIN-DT (%1) не является датой" "Значение параметра BEGIN-DT не является датой"
	 * @description RUM "Значение параметра BEGIN-DT (%1) не является датой" "Значение параметра BEGIN-DT не является датой"
	 */
	public static ServerMessage incorrectBeginDT(String beginDT) { 
		MessageParameters p = new MessageParameters();
		p.addRawString(beginDT);
		return getInstance().getMessage(INCORRECT_BEGIN_DT, p);
	}
	
	
	
	/**
	 * @message BEGIN_DT_NOT_EXCEED
	 * @description ENG "Значение BEGIN-DT (%1) не может превышать дату запуска пакетного процесса"
	 * @description RUS "Значение BEGIN-DT (%1) не может превышать дату запуска пакетного процесса"
	 * @description RUM "Значение BEGIN-DT (%1) не может превышать дату запуска пакетного процесса"
	 */
	public static ServerMessage beginDtNotExceed(String beginDT) { 
		MessageParameters p = new MessageParameters();
		p.addRawString(beginDT);
		return getInstance().getMessage(BEGIN_DT_NOT_EXCEED, p);
	}
	
	/**
	 * @message INCORRECT_END_DT
	 * @description ENG "Значение параметра END-DT (%1) не является датой" "Значение параметра END-DT не является датой"
	 * @description RUS "Значение параметра END-DT (%1) не является датой" "Значение параметра END-DT не является датой"
	 * @description RUM "Значение параметра END-DT (%1) не является датой" "Значение параметра END-DT не является датой"
	 */
	public static ServerMessage incorrectEndDT(String endDT) { 
		MessageParameters p = new MessageParameters();
		p.addRawString(endDT);
		return getInstance().getMessage(INCORRECT_END_DT, p);
	}
	
	
	
	/**
	 * @message END_DT_NOT_EXCEED
	 * @description ENG "Значение END-DT (%1) не может превышать дату запуска пакетного процесса"
	 * @description RUS "Значение END-DT (%1) не может превышать дату запуска пакетного процесса"
	 * @description RUM "Значение END-DT (%1) не может превышать дату запуска пакетного процесса"
	 */
	public static ServerMessage endDtNotExceed(String endDT) { 
		MessageParameters p = new MessageParameters();
		p.addRawString(endDT);
		return getInstance().getMessage(END_DT_NOT_EXCEED, p);
	}
		
	/**
	 * @message NO_BF_CD
	 * @description ENG "Фактор расчёта с кодом (%1) не существует" "Фактор расчёта с кодом не существует"
	 * @description RUS "Фактор расчёта с кодом (%1) не существует" "Фактор расчёта с кодом не существует"
	 * @description RUM "Фактор расчёта с кодом (%1) не существует" "Фактор расчёта с кодом не существует"
	 */
	public static ServerMessage noBfCd(String field) {
		MessageParameters p = new MessageParameters();
		p.addRawString(field);
		return getInstance().getMessage(NO_BF_CD, p);
	}	
	

	/**
	 * @message NO_CHAR_TYPE
	 * @description ENG "Тип Характеристики (%1) не найден" "Тип Характеристики (%1) не найден"
	 * @description RUS "Тип Характеристики (%1) не найден" "Тип Характеристики (%1) не найден"
	 * @description RUM "Тип Характеристики (%1) не найден" "Тип Характеристики (%1) не найден"
	 */
	public static ServerMessage noCharType(String charType) {
		MessageParameters p = new MessageParameters();
		p.addRawString(charType);
		return getInstance().getMessage(NO_CHAR_TYPE, p);
	}		
	
	
	/**
	 * @message NOT_EXISTS_CHAR_OBJECT
	 * @description ENG "Типу характеристики (%1) не назначен объект (%2)" "Типу характеристики (%1) не назначен объект (%2)"
	 * @description RUS "Типу характеристики (%1) не назначен объект (%2)" "Типу характеристики (%1) не назначен объект (%2)"
	 * @description RUM "Типу характеристики (%1) не назначен объект (%2)" "Типу характеристики (%1) не назначен объект (%2)"
	 */
	public static ServerMessage notExistsCharObject(String ch, String object) {
		MessageParameters p = new MessageParameters();
		p.addRawString(ch);
		p.addRawString(object);
		return getInstance().getMessage(NOT_EXISTS_CHAR_OBJECT, p);
	}
		

	/**
	 * @message NOT_EXISTS_ACCT_ID
	 * @description ENG "Лицевой счёт (%1) не найден" "Лицевой счёт (%1) не найден"
	 * @description RUS "Лицевой счёт (%1) не найден" "Лицевой счёт (%1) не найден"
	 * @description RUM "Лицевой счёт (%1) не найден" "Лицевой счёт (%1) не найден"
	 */
	public static ServerMessage notExistsAcctId(String acctId) {
		MessageParameters p = new MessageParameters();
		p.addRawString(acctId);
		return getInstance().getMessage(NOT_EXISTS_ACCT_ID, p);
	}		
	
	
	/**
	 * @message RECALCULATION_IMPOSSIBLE
	 * @description ENG "Перерасчёт лицевого счёта (%1) не возможен. Есть неотменённый сегмент счёта" "Перерасчёт лицевого счёта (%1) не возможен. Есть неотменённый сегмент счёта"
	 * @description RUS "Перерасчёт лицевого счёта (%1) не возможен. Есть неотменённый сегмент счёта" "Перерасчёт лицевого счёта (%1) не возможен. Есть неотменённый сегмент счёта"
	 * @description RUM "Перерасчёт лицевого счёта (%1) не возможен. Есть неотменённый сегмент счёта" "Перерасчёт лицевого счёта (%1) не возможен. Есть неотменённый сегмент счёта"
	 */
	public static ServerMessage recalculationImpossible(String acctId) {
		MessageParameters p = new MessageParameters();
		p.addRawString(acctId);
		return getInstance().getMessage(RECALCULATION_IMPOSSIBLE, p);
	}
	
	
	
	/**
	 * @message NO_FACTOR_CALCULATION_ON_DATE
	 * @description ENG "Для фактора расчёта (%1) не найдено значение на дату (%2)" "Для фактора расчёта (%1) не найдено значение на дату (%2)"
	 * @description RUS "Для фактора расчёта (%1) не найдено значение на дату (%2)" "Для фактора расчёта (%1) не найдено значение на дату (%2)"
	 * @description RUM "Для фактора расчёта (%1) не найдено значение на дату (%2)" "Для фактора расчёта (%1) не найдено значение на дату (%2)"
	 */
	public static ServerMessage noFactorCalculationOnDate(String factorCalc, String dateCalc) {
		MessageParameters p = new MessageParameters();
		p.addRawString(factorCalc);
		p.addRawString(dateCalc);		
		return getInstance().getMessage(NO_FACTOR_CALCULATION_ON_DATE, p);
	}	
		
	
	/**
	 * @message NO_EXISTS_CHAR_VAL_SAT 
	 * @description ENG "Значение (%1) для типа характеристики(%2) не найдено" "Значение (%1) для типа характеристики(%2) не найдено"
	 * @description RUS "Значение (%1) для типа характеристики(%2) не найдено" "Значение (%1) для типа характеристики(%2) не найдено"
	 * @description RUM "Значение (%1) для типа характеристики(%2) не найдено" "Значение (%1) для типа характеристики(%2) не найдено"
	 */
	public static ServerMessage notExistsCharValSat(String charValSat, String charTypeSatCd) {
		MessageParameters p = new MessageParameters();
		p.addRawString(charValSat);
		p.addRawString(charTypeSatCd);		
		return getInstance().getMessage(NO_EXISTS_CHAR_VAL_SAT, p);
	}	
	
	
	/**
	 * @message INCORRECT_CHAR_TYPE_ADJ_DUE
	 *  
	 * @description ENG "Корректировка (%1) имеет не корректное значение характеристики типа (%2)" "Корректировка (%1) имеет не корректное значение характеристики типа (%2)"
	 * @description RUS "Корректировка (%1) имеет не корректное значение характеристики типа (%2)" "Корректировка (%1) имеет не корректное значение характеристики типа (%2)"
	 * @description RUM "Корректировка (%1) имеет не корректное значение характеристики типа (%2)" "Корректировка (%1) имеет не корректное значение характеристики типа (%2)"
	 */
	public static ServerMessage incorrectCharTypeAdjDue(String sublingID, String charTypeAdjDue) {
		MessageParameters p = new MessageParameters();
		p.addRawString(sublingID);
		p.addRawString(charTypeAdjDue);		
		return getInstance().getMessage(INCORRECT_CHAR_TYPE_ADJ_DUE, p);
	}		

	/**
	 * @message ERROR_ACCOUNT_ID
	 *  
	 * @description ENG "Ошибка для л/с (%1): (%2%3%4%5%6%7%8%9%10%11%12%13%14%15%16)" "Ошибка для л/с (%1): (%2%3%4%5%6%7%8%9%10%11%12%13%14%15%16)"
	 * @description RUS "Ошибка для л/с (%1): (%2%3%4%5%6%7%8%9%10%11%12%13%14%15%16)" "Ошибка для л/с (%1): (%2%3%4%5%6%7%8%9%10%11%12%13%14%15%16)"
	 * @description RUM "Ошибка для л/с (%1): (%2%3%4%5%6%7%8%9%10%11%12%13%14%15%16)" "Ошибка для л/с (%1): (%2%3%4%5%6%7%8%9%10%11%12%13%14%15%16)"
	 */
	public static ServerMessage errorAccountId(String accountId, String errorMessage) {
		MessageParameters p = new MessageParameters();
		p.addRawString(accountId);
		addLongString(p, errorMessage, 15);		
		return getInstance().getMessage(ERROR_ACCOUNT_ID, p);
	}		
	
	
	public static void addLongString(MessageParameters p, String longString, int max)
	  {
	    String[] a = splitForMessage(longString);
	    for (int i = 0; i < max; i++) {
	      if (i >= a.length) {
	        p.addMissingParameter();
	      } else {
	        p.addRawString(a[i]);
	      }
	    }
	  }
	  
	  public static String[] splitForMessage(String path)
	  {
	    int size = 30;
	    String[] result = new String[(path.length() - path.length() % 30) / 30 + 1];
	    int i = 0;
	    while (i < result.length)
	    {
	      result[i] = path.substring(i * 30, Math.min(path.length(), (i + 1) * 30));
	      i++;
	    }
	    return result;
	  }	
}
