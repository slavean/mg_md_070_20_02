package com.splwg.cm.domain.admin.serviceAgreementType;

import com.splwg.base.api.datatypes.Bool;
import com.splwg.base.api.datatypes.Date;
import com.splwg.ccb.api.lookup.FinancialTransactionTypeLookup;
import com.splwg.ccb.domain.admin.adjustmentType.entity.AdjustmentType_Id;
import com.splwg.ccb.domain.admin.serviceAgreementType.algorithm.ftFreeze.SaTypeFtFreezeAlgorithmSpot;
import com.splwg.ccb.domain.customerinfo.account.entity.Account_Id;
import com.splwg.ccb.domain.customerinfo.account.entity.CreditReviewSchedule;
import com.splwg.ccb.domain.customerinfo.account.entity.CreditReviewSchedule_DTO;
import com.splwg.ccb.domain.customerinfo.account.entity.CreditReviewSchedule_Id;
import com.splwg.ccb.domain.financial.financialTransaction.entity.FinancialTransaction;
import com.splwg.shared.logging.Logger;
import com.splwg.shared.logging.LoggerFactory;

/**
*
*  @AlgorithmComponent (softParameters = { @AlgorithmSoftParameter (name = typeCorr, required = false, type = string)
*            , @AlgorithmSoftParameter (name = dayMonth, required=true, type = string)
*         })
*
* @algorithm CM-SHD-ADM FTFZ
* @algorithmDescription ENG "Планирование проверки ДЗ" "Алгоритм планирует проверку ДЗ на основании утверждения ФТ корректировки.
* @algorithmDescription RUS "Планирование проверки ДЗ" "Алгоритм планирует проверку ДЗ на основании утверждения ФТ корректировки.
* @algorithmDescription ROU "Планирование проверки ДЗ" "Алгоритм планирует проверку ДЗ на основании утверждения ФТ корректировки.
* 
* @parameter 1 N
* @parameterDescription 1 ENG "Тип корректировки"
* @parameterDescription 1 RUS "Тип корректировки"
* @parameterDescription 1 ROU "Тип корректировки"
* @parameter 2 Y
* @parameterDescription 2 ENG "Число месяца"
* @parameterDescription 2 RUS "Число месяца"
* @parameterDescription 2 ROU "Число месяца"
* 
*/

public class CmShdAdmAlgComp_Impl extends CmShdAdmAlgComp_Gen implements
		SaTypeFtFreezeAlgorithmSpot {
	Logger logger = LoggerFactory.getLogger(CmShdAdmAlgComp_Impl.class);

	boolean validationPassed = false;

	private FinancialTransaction financialTransaction;

	private Bool isAdded = Bool.FALSE;
	

	@Override
	public void invoke() {
		
		
		String typeCorrection = getTypeCorr();
		String nrDay = getDayMonth();
		int d = 0;
		
		//PreparedStatement ps = null;
		//try {
			if (!isBlankOrNull(typeCorrection)) {

				AdjustmentType_Id adj = new AdjustmentType_Id(typeCorrection);
				
				if (adj.getEntity() == null)
/*					
				ps = createPreparedStatement("select ADJ_TYPE_CD from CI_ADJ "
						+ "where ADJ_TYPE_CD = '" + typeCorrection + "'");

				List<SQLResultRow> adjList = ps.list();
				if (adjList.size() == 0) 
				*/
				
				{
					logger.info("validation failed");
					addError(CmShdAdmAlgMessageRepository
							.noTypeCorrection(typeCorrection));
				}
			}

			try {
				d = Integer.valueOf(nrDay);
				if ((d < 1) || (d > 31)) {
					logger.info("validation failed");
					addError(CmShdAdmAlgMessageRepository
							.invalidNumberDay(nrDay));
				}
			} catch (NumberFormatException e) {
				logger.info("validation failed (not number)");
				addError(CmShdAdmAlgMessageRepository.invalidNumberDay(nrDay));
			}
		/*
		} finally {
			if (ps != null)
				ps.close();
		}
*/
			
		logger.info("Transaction type = ["
				+ financialTransaction.getFinancialTransactionType().value()
				+ "]");
		logger.info("Parent_id = ["
				+ financialTransaction.getParentId()
				+ "]");
		
		if (!financialTransaction.getFinancialTransactionType().value().trim().equals("AD"))
		{
			logger.info("2.1.	Если тип ФТ не равен AD, то завершить выполнение программы.");
			return;
		}
		if ((!isBlankOrNull(typeCorrection)) && (!financialTransaction.getParentId().trim().equals(typeCorrection))) 
		{
			logger.info("2.2.	Если S1 задан и  PARENT_ID не равно S1, то завершить выполнение программы");
			return;
		}
		
		Date dt = financialTransaction.getAccountingDate();
		
		dt = dt.addDays(-dt.getDay()+1);
		dt = dt.addMonths(2);
		dt = dt.addDays(-1);
		
		logger.info("ACCOUNTING_DATE = ["+financialTransaction.getAccountingDate().toString()+"]");
		logger.info("ACCOUNTING_DATE modified = ["+dt.toString()+"]");
		
		int day;
		if (d<dt.getDay()) day = d;
		else day = dt.getDay();
		
		int month = dt.getMonth();
		
		int year = dt.getYear();
		
		Date newDate = new Date(year, month, day);
		
		logger.info("New date = ["+newDate.toString()+"]");
		
		//ps = null;
		//try {
			
				Account_Id accId = new Account_Id(financialTransaction.getServiceAgreement().getAccount().getId().getIdValue());
				CreditReviewSchedule_Id crsId =new CreditReviewSchedule_Id(accId, newDate);
				
				
				
				if (crsId.getEntity()==null)
				{
					
					logger.info("will be insert - ["+financialTransaction.getServiceAgreement().getAccount().getId().getIdValue()+ ", "+newDate.toString()+", 1]");
					
					CreditReviewSchedule_DTO crsDTO = (CreditReviewSchedule_DTO) createDTO(CreditReviewSchedule.class);					
					crsDTO.setId(new CreditReviewSchedule_Id(accId, newDate));
					crsDTO.setVersion(1);
					
					CreditReviewSchedule crs = crsDTO.newEntity();
					isAdded = Bool.TRUE;
					logger.info(crs.entityName());
					return;
					
				}
				else
				{
					logger.info("5.	Если строка найдена, то выйти из программы");
					return;
				}
				
/*				
			
				ps = createPreparedStatement("select * from  CI_ADM_RVW_SCH where ACCT_ID = '"+
				financialTransaction.getServiceAgreement().getId().getIdValue()+"' and NEXT_CR_RVW_DT = :DATE");
				ps.bindDate("DATE", newDate);
				
				List<SQLResultRow> adjList = ps.list();
				if (adjList.size() == 0) {
					logger.info("will be insert");
				}
				else
				{
					logger.info("5.	Если строка найдена, то выйти из программы");
					return;
				}
			
		} finally {
			if (ps != null)
				ps.close();
		} */
	}

	@Override
	public Bool getWasFinancialProcessAdded() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setFinancialTransaction(FinancialTransaction arg0) {
		// TODO Auto-generated method stub
		this.financialTransaction = arg0;

	}

	@Override
	public void setFinancialTransactionType(FinancialTransactionTypeLookup arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setRegularFinancialTransaction(FinancialTransaction arg0) {
		// TODO Auto-generated method stub

	}

}
