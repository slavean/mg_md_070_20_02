package com.splwg.cm.domain.admin.serviceAgreementType;

import com.splwg.base.domain.common.message.MessageParameters;
import com.splwg.shared.common.ServerMessage;

/**
 * @messageRepository
 */
public class CmShdAdmAlgMessageRepository extends CmShdAdmAlgMessages {

	private static CmShdAdmAlgMessageRepository instance = new CmShdAdmAlgMessageRepository();

	private static CmShdAdmAlgMessageRepository getInstance() {
		return instance;
	}


	/**
	 * @message NO_TYPE_CORRECTION
	 * @description ENG "Тип корректировки %1 не существует" " "
	 * @description RUS "Тип корректировки %1 не существует" " "
	 * @description ROU "Тип корректировки %1 не существует" " "
	 */
	public static ServerMessage noTypeCorrection(String typeCorrection) 
	{
		MessageParameters params = new MessageParameters();
		params.addRawString(typeCorrection);
		return getInstance().getMessage(NO_TYPE_CORRECTION, params);		
	}
	
	/**
	 * @message INVALID_NUMBER_DAY
	 * @description ENG "Неверно задано число месяца %1" " "
	 * @description RUS "Неверно задано число месяца %1" " "
	 * @description ROU "Неверно задано число месяца %1" " "
	 */
	public static ServerMessage invalidNumberDay(String nrDay) 
	{
		MessageParameters params = new MessageParameters();
		params.addRawString(nrDay);
		return getInstance().getMessage(INVALID_NUMBER_DAY, params);		
	}
}
